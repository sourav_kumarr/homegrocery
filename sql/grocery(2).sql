-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2016 at 10:44 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `grocery`
--

-- --------------------------------------------------------

--
-- Table structure for table `grocery_category`
--

CREATE TABLE IF NOT EXISTS `grocery_category` (
  `cat_id` bigint(100) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(100) NOT NULL,
  `cat_parent_id` int(100) NOT NULL,
  `cat_link` varchar(100) NOT NULL,
  `cat_sort_order` bigint(100) NOT NULL,
  `cat_loc_id` varchar(255) NOT NULL,
  `cat_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(255) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `grocery_category`
--

INSERT INTO `grocery_category` (`cat_id`, `cat_name`, `cat_parent_id`, `cat_link`, `cat_sort_order`, `cat_loc_id`, `cat_created_at`, `created_by`) VALUES
(17, 'DRINK AND BEVERAGES', 0, '#', 1, '1', '2016-09-13 08:14:49', '1'),
(18, 'GULUCOSE', 17, 'gulucose', 1, '1', '2016-09-13 08:14:49', '1'),
(19, 'SOUPES', 17, 'soupes', 1, '1', '2016-09-13 08:14:49', '1'),
(20, 'SODA', 17, 'soda', 1, '1', '2016-09-13 08:14:49', '1'),
(21, 'BREAKFAST AND CEREALS', 0, '#', 2, '1', '2016-09-13 08:14:49', '1'),
(22, 'FLAKES,CHOCOS,LOOPS', 21, 'flakes,chocos,loops', 2, '1', '2016-09-13 08:14:49', '1'),
(23, 'OATS', 21, 'oats', 2, '1', '2016-09-13 08:14:49', '1'),
(24, 'MOUSELI', 21, 'mouseli', 2, '1', '2016-09-13 08:14:49', '1'),
(25, 'SAUCES,JAMS AND PICKELES', 0, '#', 3, '1', '2016-09-13 08:14:49', '1'),
(26, 'PICKLES', 25, 'pickles', 3, '1', '2016-09-13 08:14:49', '1'),
(27, 'SAUCES', 25, '#', 3, '1', '2016-09-13 12:18:52', '1'),
(28, 'OTHER JAMS', 25, 'other jams', 3, '1', '2016-09-13 08:14:49', '1'),
(29, 'JAM', 27, 'jam', 3, '1', '2016-09-13 12:25:20', '1'),
(30, 'JAM 1', 27, 'jam 1', 3, '1', '2016-09-13 12:28:40', '1');

-- --------------------------------------------------------

--
-- Table structure for table `grocery_keys`
--

CREATE TABLE IF NOT EXISTS `grocery_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `grocery_keys`
--

INSERT INTO `grocery_keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 1, 'abc', 1, 0, 0, '127.0.0.1,192.168.1.139', '2016-09-14 06:43:43');

-- --------------------------------------------------------

--
-- Table structure for table `grocery_location`
--

CREATE TABLE IF NOT EXISTS `grocery_location` (
  `loc_id` int(100) NOT NULL AUTO_INCREMENT,
  `loc_name` text NOT NULL,
  `loc_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `loc_created_by` bigint(100) NOT NULL,
  PRIMARY KEY (`loc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `grocery_location`
--

INSERT INTO `grocery_location` (`loc_id`, `loc_name`, `loc_created_at`, `loc_created_by`) VALUES
(1, 'Chandigarh', '2016-09-13 08:07:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `grocery_products`
--

CREATE TABLE IF NOT EXISTS `grocery_products` (
  `prod_id` bigint(100) NOT NULL AUTO_INCREMENT,
  `prod_name` varchar(100) NOT NULL,
  `prod_desc` text NOT NULL,
  `prod_price` bigint(100) NOT NULL,
  `prod_discount_price` bigint(100) NOT NULL,
  `prod_unit` varchar(100) NOT NULL,
  `prod_quantity` bigint(100) NOT NULL,
  `prod_cat_id` bigint(100) NOT NULL,
  `prod_off_percentage` varchar(100) NOT NULL,
  `prod_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `prod_created_by` bigint(100) NOT NULL,
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `grocery_products`
--

INSERT INTO `grocery_products` (`prod_id`, `prod_name`, `prod_desc`, `prod_price`, `prod_discount_price`, `prod_unit`, `prod_quantity`, `prod_cat_id`, `prod_off_percentage`, `prod_created_at`, `prod_created_by`) VALUES
(1, 'GLUCON-D NIMBU PANI 500GM', 'GLUCON-D IS THE PREFERRED CHOICE IN SUMMER WHEN THE SCORCHING HEAT DRAINS OUT BODY GLUCOSE. GLUCON-D CONTAINS 99.4% GLUCOSE. IT IS EASILY ABSORBED BY BODY, THUS GIVING INSTANT ENERGY & REJUVENATION.', 140, 135, 'GM', 500, 18, '10% off', '2016-09-14 08:29:47', 1),
(2, 'GLUCON-D AAM PANA 500GM', 'GLUCON-D AAM PANA 500GM\r\nGlucon-D is the preferred choice in summer when the scorching heat drains out body glucose. Glucon-D contains 99.4 percent glucose. It is easily absorbed by body, thus giving instant energy and rejuvenation. It restores energy 2 times faster compared to ordinary drinks.', 140, 135, 'GM', 500, 18, '', '2016-09-14 08:29:47', 1),
(3, 'CHINGS INSTANT SOUP MANCHOW 15GM', 'CHINGS INSTANT SOUP MANCHOW 15GM\r\nSoup is an integral part of every Chinese meal, and is chosen to compliment the dishes being served. Soups provide a textural contrast,', 10, 10, 'GM', 15, 19, '', '2016-09-14 08:28:04', 1),
(4, 'CHINGS INSTANT SOUP MIX VEG 15GM', 'CHINGS INSTANT SOUP MIX VEG 15GM\r\nSoup is an integral part of every Chinese meal, and is chosen to compliment the dishes being served. Soups provide a textural contrast,', 10, 10, 'GM', 15, 19, '', '2016-09-14 08:29:03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `grocery_products_images`
--

CREATE TABLE IF NOT EXISTS `grocery_products_images` (
  `image_id` bigint(100) NOT NULL AUTO_INCREMENT,
  `image_link` text NOT NULL,
  `image_prod_id` bigint(100) NOT NULL,
  `image_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `grocery_products_images`
--

INSERT INTO `grocery_products_images` (`image_id`, `image_link`, `image_prod_id`, `image_created_at`) VALUES
(1, 'assets/images/products/gulucose0.jpg', 1, '2016-09-14 08:22:41'),
(2, 'assets/images/products/gulucose1.jpg', 2, '2016-09-14 08:22:49'),
(3, 'assets/images/products/soups0.png', 3, '2016-09-14 08:22:56'),
(4, 'assets/images/products/soups1.png', 4, '2016-09-14 08:23:04');

-- --------------------------------------------------------

--
-- Table structure for table `grocery_user`
--

CREATE TABLE IF NOT EXISTS `grocery_user` (
  `user_id` bigint(100) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `user_address_1` text NOT NULL,
  `user_address_2` text NOT NULL,
  `user_city` varchar(100) NOT NULL,
  `user_state` varchar(100) NOT NULL,
  `user_country` varchar(100) NOT NULL,
  `user_pincode` varchar(100) NOT NULL,
  `user_mobile` varchar(100) NOT NULL,
  `user_status` varchar(100) NOT NULL,
  `user_otp` varchar(100) NOT NULL,
  `user_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `grocery_user`
--

INSERT INTO `grocery_user` (`user_id`, `user_name`, `user_email`, `user_password`, `user_type`, `user_address_1`, `user_address_2`, `user_city`, `user_state`, `user_country`, `user_pincode`, `user_mobile`, `user_status`, `user_otp`, `user_date`) VALUES
(1, 'sourav', 'nk3527@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '', '', '', '', '', '', '8146499783', 'A', '', '2016-09-14 14:26:50'),
(2, 'sourav', 'nk3527@gmail.com', '12345', '', '', '', '', '', '', '', '9041634625', 'D', '446215', '2016-09-14 15:27:31'),
(3, 'sourav', 'nk3527@gmail.com', '12345', '', '', '', '', '', '', '', '9041634625', 'D', '145121', '2016-09-14 15:33:44'),
(4, 'sourav', 'nk3527@gmail.com', '12345', '', '', '', '', '', '', '', '9041634625', 'D', '431308', '2016-09-14 15:34:59'),
(8, 'sourav', 'sksh3527@gmail.com', '12345', '', '', '', '', '', '', '', '8146499783', 'D', '354970', '2016-09-14 15:46:26');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
