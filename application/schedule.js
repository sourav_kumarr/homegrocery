/**
 * Created by Sourav on 7/22/2017.
 */

var parent_id = 0;
var level_id = 1;
function onLevelLoaded1(step,id,path) {
    console.log('step -- '+step);
    step++;
    $(".folders").removeClass('active');
    $(".folders.folders-"+step).addClass('active');
    parent_id = id;
    folder_path = decodeURIComponent(path).replace(/\++/g, ' ')+'/';
    level_id = step;


}

function onBack() {
    level_id--;
    $(".folders").removeClass('active');
    $(".folders.folders-"+level_id).addClass('active');

}
function loadFolderData1() {
    console.log('loadFolderData1--- '+parent_id);
    var url = 'api/folderProcess.php';
    $.post(url,{parent_id:parent_id,type:'getFolderData'},function(data){
        console.log(JSON.stringify(data));
        var status = data.Status;
        var message = data.Message;

        if(status == 'Success') {
            var data_ = '<thead><tr><th>Thumbnail</th><th>Name</th><th>Action</th></tr></thead><tbody>';
            var folders = data.folders;
            var videos = data.videos;
            var counter = 1;
            // load folders data


            for(var i=0;i<folders.length;i++) {
                data_ = data_+'<tr><td ><i class="fa fa-2x fa-folder" style="cursor: pointer" onclick=onLevelLoaded1("'+level_id+'","'+folders[i].folder_id+'","'+encodeURI(folders[i].folder_path)+'");loadFolderData1();></i> </td><td ><span style="cursor:pointer" onclick=onLevelLoaded1("'+level_id+'","'+folders[i].folder_id+'","'+encodeURI(folders[i].folder_path)+'");loadFolderData1();>'+folders[i].folder_name+'</span></td>' +
                    '<td><span>NA</span></td></tr>';
                counter++;
            }

            // load videos data

            for(var i=0;i<videos.length;i++) {
                var video_name = videos[i].video_name.split(";")[1].split(".mp4")[0];
                var date = toHumanFormat(videos[i].added_on);
                data_ = data_+'<tr><td><i class="fa fa-2x fa-video-camera"></i></td><td> '+video_name+'</td>' +
                    '<td data-title="Action"><input class="btn btn-info btn-sm" id="btn'+videos['video_id']+'" type="button" value="Select" onclick=selection("btn'+videos[i].video_id+'","'+videos[i].video_id+'","'+encodeURI(videos[i].video_name)+'","'+encodeURI(videos[i].video_path)+'") /> </td></tr>';

                counter++;
            }

            if(level_id == 1)
                $("#table1").html(data_);
            else if(level_id == 2)
                $("#table2").html(data_);
            else if(level_id == 3)
                $("#table3").html(data_);



        }
        else if(status == 'Error') {
            if(level_id == 2)
                $("#datatable-buttons1").html(message);
            else if(level_id == 3)
                $("#datatable-buttons2").html(message);
        }

    });

}
loadFolderData1();
