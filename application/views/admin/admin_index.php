<html>
<title><?php echo $title;?></title>
<style>
    .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 5px;height:20px; }
    .toggle.ios .toggle-handle { border-radius: 5px;height:20px; }
</style>
<script type="text/javascript" src="<?php echo base_url('assets/js/admin_index.js')?>"></script>
<div class="row">
<div class="col-md-12">
    <div class="col-md-4 ordercounttop">
        <img src="<?php echo base_url('assets/images/icons/orders.png')?>" class="ordercounttopimg" />
        <div class="ordernumber"><span class="rednumber" id="processing">0</span> In-Process Orders <br><span class="rednumber" id="completed">0</span> Ready to Ship Orders</div>
    </div>
    <div class="col-md-4" style="margin-top: 30px;text-align:center">
        <label class="newcustomreq">New Custom Request</label><br>
        <label class="show">Show&nbsp;<label id="showdiv"></label></label>
    </div>
    <div class="col-md-4" style="margin-top: 30px;text-align:right">
        <label class="processing" ><i class="fa fa-clock-o"></i>&nbsp;Processing </label>&nbsp;
        <label class="processing" style="color:black" ><i class="fa fa-truck"></i>&nbsp;Ready to Ship </label>&nbsp;
        <label class="completed" ><i class="fa fa-check-circle-o"></i>&nbsp;completed </label><br>
        <div class="exportdiv"><div id="searchdiv"></div><input type="button" value="Export to Excel" class="exportbtn" /><i class='fa fa-file-excel-o fa-2x faexcel'></i></div>
    </div>
</div>
</div>
<div class="row">

</div>
<!--<div class="col-md-12 btn-container loader"  id="load" >
    <img src="<?php /*echo base_url('assets/images/panel/loader.gif')*/?>" width="100" height="90"   />
</div>
-->
<div class="col-md-12 btn-container" >
  <div class="custom_loader" id="loader"></div>
</div>
<div class="col-md-12 " id="dynamic_table">
</div>
<div class="alert success" id="success">
    <span class="closebtn" >&times;</span>
    <strong>Success!</strong> Indicates a successful or positive action.
</div>

<div class="alert error" id="error">
    <span class="closebtn" >&times;</span>
    <strong>Success!</strong> Indicates a successful or positive action.
</div>

</html>