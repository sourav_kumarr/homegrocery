<html>
<script type="text/javascript" src="<?php echo base_url('assets/js/order_detail.js')?>"></script>
<link href="<?php echo base_url('assets/css/order_detail.css')?>" rel="stylesheet">

<title><?php echo $title;?></title>

<style>
    .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 5px;height:20px; }
    .toggle.ios .toggle-handle { border-radius: 5px;height:20px; }
</style>

<!--<div class="col-md-12 btn-container loader"  id="load" >
    <img src="<?php /*echo base_url('assets/images/panel/loader.gif')*/?>" width="100" height="90"   />
</div>
-->
<!--<div class="col-md-12 btn-container" >
    <div class="custom_loader" id="loader"></div>
</div>-->
<!--<div class="col-md-12 " id="dynamic_table">
</div>-->
<?php
if(!isset($order_data)) {
    echo "<label>order not found</label>";
}
else {
//     print_r($order_items);
    ?>
    <div class="page-container">
        <div class="order-container">
            <div class="order-header">
                <div class="order-title">
                    <p>Order Product's</p>
                </div>
            </div>
            <div class="order-number">
                <p>ORDER INFORMATION - <?php echo $order_data->order_number;?></p>
            </div>
            <div class="order-items">
                <div class="order-item-detail">
                    <div class="item0">
                        <label>Order Items : </label>
                    </div>
                    <div class="item1">
                        <label>1 Item</label>
                    </div>
                </div>
                <div class="order-item-detail">
                    <div class="item0">
                        <label>Order  Date : </label>
                    </div>
                    <div class="item1">
                        <label><?php echo $order_data->order_date;?></label>
                    </div>
                </div>

                <div class="order-item-detail">
                    <div class="item0">
                        <label>Order Status : </label>
                    </div>
                    <div class="item1">
                        <label><?php echo strtoupper($order_data->order_state);?></label>
                    </div>
                </div>
                <div class="order-item-detail" style="border-bottom:2px lightgray solid">
                    <div class="item0">
                        <label>Order Payment Status : </label>
                    </div>
                    <div class="item1">
                        <label><?php echo strtoupper($order_data->order_payment_status)?></label>
                    </div>
                </div>
            </div>
            <?php
            if(!isset($order_items)){
             echo "<div class='order-number'><p>No products found in this order</p></div>";
            }
            else {
                 $data = $order_items['data'];
                 $total = 0;
                 if(sizeof($data)>0) {


                     ?>
                     <div class="order-number">
                         <p>PRODUCT INFORMATION</p>
                     </div>
                     <table class="table table-bordered table-hover" style="margin-left: 3%;width: 90%">
                         <thead>
                         <tr>
                             <td>Name</td>
                             <td>Quantity</td>
                             <td>Price</td>
                             <td>Total</td>

                         </tr>
                         </thead>
                         <tbody>
                         <?php
                          for($i = 0;$i<sizeof($data);$i++){
                              $total= $total+ intval($data[$i]->det_price)*intval($data[$i]->det_quantity);
                              ?>
                              <tr>
                                  <td><?php echo $data[$i]->det_product_name?></td>
                                  <td><?php echo $data[$i]->det_quantity.'*'.$data[$i]->det_price;?></td>
                                  <td><?php echo $data[$i]->det_price?></td>
                                  <td><?php echo intval($data[$i]->det_price)*intval($data[$i]->det_quantity);?></td>
                             </tr>
                         <?php
                          }
                         ?>

                         </tbody>
                     </table>
                     <div class="order-bottom">
                         <label>Shipping charges : RS 10</label>
                         <label>Service tax : RS 10</label>
                         <label class="order-total">Total : RS <?php echo $total+20;?></label>

                     </div>
                     <?php
                 }
                else{
                    echo "<div class='order-number'><p>No products found in this order</p></div>";
                }
            }
           ?>
        </div>
    </div>
    <?php
}
?>
<div class="alert success" id="success">
    <span class="closebtn" >&times;</span>
    <strong>Success!</strong> Indicates a successful or positive action.
</div>

<div class="alert error" id="error">
    <span class="closebtn" >&times;</span>
    <strong>Success!</strong> Indicates a successful or positive action.
</div>

</html>