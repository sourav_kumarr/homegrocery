<html>
<title><?php echo $title;?></title>
<script type="text/javascript" src="<?php echo base_url("assets/js/category.js");?>"></script>

<link href="<?php echo base_url('assets/css/order_state.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/category.css')?>" rel="stylesheet">

<body>
<div class="btn-container" style="margin-top: 10px">
    <div class="custom_loader"></div>
</div>
<div class="page-container" id="main-content">
    <div class="order-container">
        <div class="page-header">
            <div class="page-left-header">
                <div class="item-image " id="state"><img src="<?php echo base_url("assets/images/icons/category.png")?>"></div>
                <div class="item">
                    <div class="item-text">
                        <label id="in-process">Sach Grocery Category  </label>
                    </div>
                    <div class="item-text" >
                        <label id="state-count">Grocery Linked With 5 Category</label>
                    </div>
                </div>

            </div>

        </div>
        <?php echo $cats['data'];?>
    </div>
</div>

<div id="myModal" class="modal">

</div>


</body>
</html>
