<html>
<title><?php echo $title;?></title>
<script>
    $(".leftmenuitems").removeClass("leftmenuitemsactive");
    $("#index").addClass("leftmenuitemsactive");

</script>
<script type="text/javascript" src="<?php echo base_url("assets/js/order_state.js");?>"></script>
<link href="<?php echo base_url('assets/css/order_state.css')?>" rel="stylesheet">
<body onload="body_loaded('<?php echo $order_type?>')">
<div class="btn-container" style="margin-top: 10px">
   <div class="custom_loader"></div>
</div>
<div class="page-container" id="main-content">
  <div class="order-container">
   <div class="page-header">
    <div class="page-left-header">
        <div class="item-image " id="state"><i class="fa fa-check-circle-o fa-4x fa completed"></i></div>
        <div class="item">
            <div class="item-text">
                <label id="in-process">In-Process Orders </label>
             </div>
            <div class="item-text" >
                <label id="state-count">1 Ready to Ship Order</label>
            </div>
       </div>

    </div>

   </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="example">
        </table>
    </div>
  </div>
</div>
<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 30-Sep-16
 * Time: 2:20 PM
 */
?>
</body>
</html>
