<html>
<title><?php echo $title;?></title>
<script>
    $(".leftmenuitems").removeClass("leftmenuitemsactive");
    $("#index").addClass("leftmenuitemsactive");

</script>
<script type="text/javascript" src="<?php echo base_url("assets/js/customers.js");?>"></script>

<link href="<?php echo base_url('assets/css/order_state.css')?>" rel="stylesheet">
<body >
<div class="btn-container" style="margin-top: 10px">
    <div class="custom_loader"></div>
</div>
<div class="page-container" id="main-content">
    <div class="order-container">
        <div class="page-header">
            <div class="page-left-header">
                <div class="item-image " id="state"><img src="<?php echo base_url("assets/images/icons/customer.png")?>"></div>
                <div class="item">
                    <div class="item-text">
                        <label id="in-process">Sach Grocery Customers  </label>
                    </div>
                    <div class="item-text" >
                        <label id="state-count">Grocery Linked With 5 Customers</label>
                    </div>
                </div>

            </div>

        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="example">
            </table>
        </div>
    </div>
</div>

<div class="alert success" id="success">
    <span class="closebtn" >&times;</span>
    <strong>Success!</strong> Indicates a successful or positive action.
</div>

<div class="alert error" id="error">
    <span class="closebtn" >&times;</span>
    <strong>Success!</strong> Indicates a successful or positive action.
</div>
</body>
</html>
