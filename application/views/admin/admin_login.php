<html>
<title>HomeGrocery</title>
<head>
<link href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet" />
<link href="<?php echo base_url('assets/css/admin.css');?>" rel="stylesheet" />
<link href=href="<?php echo base_url('assets/css/font-awesome.css');?>" rel="stylesheet" />

<script src="<?php echo base_url('assets/js/bootstrap.js');?>" ></script>
<script src="<?php echo base_url('assets/js/jquery-2.2.1.min.js');?>" ></script>
<script src="<?php echo base_url('assets/js/login.js');?>" ></script>
</head>
<body>
<div class="col-md-12" style="padding:0">
    <div class="col-md-12 insidediv">
        <div class="col-md-6 login_left">
            <img src="<?php echo base_url('assets/images/panel/ipad12.png')?>" class="login_left_img"/>
        </div>
        <div class="col-md-5 login_right">
            <img src="<?php echo base_url('assets/images/panel/sach_grocery.png')?>" class="login_logo"/>
            <div class="col-md-12" style="text-align:center">
                <form method="post" id="login">
                    <input type="hidden" name="block" value="login" />
                    <div class="form-group" >
                        <input required class="custom_login_feild" type="email" value="" name="email" id="email" Placeholder=" Enter E-Mail" />
                        <i class="fa fa-envelope fa-2x" style="color:#ccc;position:absolute;left:55px;margin-top:35px"></i>
                    </div>
                    <div class="form-group" >
                        <input required class="custom_login_feild" type="password" value="" name="password" Placeholder=" Enter Password" id="password" />
                        <i class="fa fa-lock fa-2x" style="color:#ccc;position:absolute;left:61px;margin-top:35px"></i>
                    </div>
                    <div class="form-group form-horizontal" >
                        <div class="btn-container">
                        <input class="custom_login_button" type="submit" value="LOGIN" />
                         <img src="<?php echo base_url('assets/images/panel/loader.gif');?>" height="100" width="70" class="loader" id="load">
                       </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--<p class="login_caption">Login</p>

</div>-->
</body>
</html>