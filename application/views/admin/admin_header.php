
<link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet" />
<link href="<?php echo base_url('assets/css/admin.css')?>" rel="stylesheet" />
<link href="<?php echo base_url('assets/css/font-awesome.css')?>" rel="stylesheet" />
<link href="<?php echo base_url('assets/css/jquery.dataTables.min.css')?>" rel="stylesheet" />
<link href="<?php echo base_url('assets/css/bootstrap-toggle.css')?>" rel="stylesheet">

<script src="<?php echo base_url('assets/js/jquery-2.2.1.min.js')?>" ></script>
<script src="<?php echo base_url('assets/js/bootstrap.js')?>" ></script>
<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>" ></script>
<script src="<?php echo base_url('assets/js/bootstrap-toggle.js')?>"></script>

<div class="col-md-12 top_header">
    <label style="float:left">
        <img src="<?php echo base_url('assets/images/panel/sach_grocery.png')?>" class="headertoplogo" />
    </label>
    <label class="headeradminname" id="dropdownmenu"><span id="welcome">Welcome,    <?php echo $user_data[0]->user_name;?>
 </span>&nbsp;<i class = "fa fa-angle-down"></i></label>
    <div class="floatingmenu" id="floatingmenu">
        <p class="floatingmenuitem"><i class="fa fa-edit"></i>&nbsp;PROFILE</p>
        <p class="floatingmenuitem"><i class="fa fa-chain"></i>&nbsp;HELP</p>
        <a href="<?php echo base_url();?>/logout/"><p class="floatingmenuitem" style="border:0"><i class="fa fa-sign-out"></i>&nbsp;LOGOUT</p></a>
    </div>
</div>
<div class="col-md-12 center_container">
    <div class="col-md-2 left_nav">
        <a href="<?php echo base_url();?>order/"><li class="leftmenuitems" id="index"><img src="<?php echo base_url('assets/images/icons/home.png')?>" class="menuitemicons" />&nbsp;Home</li></a>
        <li class="leftmenuitems" id="orders"><img src="<?php echo base_url('assets/images/icons/orders.png')?>" class='menuitemicons' /><a href='orders/'>&nbsp;Orders(<span class="ordercount"><?php if(isset($order_count)) echo $order_count;?></span>)</a><span class="plusicon" id="plusicon"><i class="fa fa-plus"></i></span></li>
        <ul class='submenu'>
            <a href="<?php echo base_url();?>order/state/open">
                <li class='submenuitems submenuactive' id='inprocess'><img class='menuitemicons' src="<?php echo base_url('assets/images/icons/in-process.png')?>"/> In-Process Orders</li>
            </a>
            <a href="<?php echo base_url();?>order/state/ship">
                <li class='submenuitems' id='readytoship'><img class='menuitemicons' src="<?php echo base_url('assets/images/icons/readytoship.png')?>"/> Ready-to-Ship</li></a>
            <a href="<?php echo base_url();?>order/state/complete">
                <li class='submenuitems' id='completedorder'><img class='menuitemicons' src="<?php echo base_url('assets/images/icons/completed.png')?>"/> Completed Orders</li></a>
            <a href="<?php echo base_url();?>order/state/cancel">
                <li class='submenuitems' id='cancelledorder'><img class='menuitemicons' src="<?php echo base_url('assets/images/icons/deleteorder.png')?>"/> Cancelled Orders</li>
            </a>
        </ul>
        <!--<a href="style.php"><li class="leftmenuitems" id="styles"><img src="<?php /*echo base_url('assets/images/icons/styles.png')*/?>" class="menuitemicons" />&nbsp;Styles</li></a>
        <a href="fabric.php"><li class="leftmenuitems" id="fabrics"><img src="<?php /*echo base_url('assets/images/icons/fabric.png')*/?>" class="menuitemicons" />&nbsp;Fabrics</li></a>
        <a href='accessory.php'><li class="leftmenuitems" id="accessory"><img src="<?php /*echo base_url('assets/images/icons/accessories.png')*/?>" class="menuitemicons" />&nbsp;Accessory</li></a>
        -->
        <a href="<?php echo base_url()?>customer"><li class="leftmenuitems" id="customers"><img src="<?php echo base_url('assets/images/icons/customer.png')?>" class="menuitemicons" />&nbsp;Customers</li></a>
        <a href="<?php echo base_url()?>categoryy"><li class="leftmenuitems" id="category"><img src="<?php echo base_url('assets/images/icons/category.png')?>" class="menuitemicons" />&nbsp;Category</li></a>

      <!--  <a href="material.php"><li class="leftmenuitems" id="materials" ><img src="<?php /*echo base_url('assets/images/icons/material.png')*/?>" class="menuitemicons" />&nbsp;Material</li></a>
        <a href="pattern.php"><li class="leftmenuitems" id="patterns"><img src="<?php /*echo base_url('assets/images/icons/pattern.png')*/?>" class="menuitemicons" />&nbsp;Pattern</li></a>
      -->
    </div>
    <div class="col-md-10 right_content">
        <!-- header end here-->
        <script>
            $(document).ready(function() {
                $("#dropdownmenu").click(function () {
                    $("#floatingmenu").toggle();
//                    alert("dropdown is clicked....");
                });
                $("#plusicon").click(function () {
                    var variclass = $("#plusicon").attr("class")
                    if (variclass == "plusicon") {
                        $("#plusicon").html("<i class='fa fa-minus'></i>");
                        $("#plusicon").removeClass("plusicon");
                        $("#plusicon").addClass("minusicon");
                        $(".submenu").slideDown("slow");
                    }
                    else if (variclass == "minusicon") {
                        $("#plusicon").html("<i class='fa fa-plus'></i>");
                        $("#plusicon").removeClass("minusicon");
                        $("#plusicon").addClass("plusicon");
                        $(".submenu").slideUp("slow");
                    }
                });
            });

        </script>
