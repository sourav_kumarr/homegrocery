<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('orders_model','',TRUE);
    $this->load->library('db_query');

  }

  function index()
  {
//    $this->load->helper('form');
    $this->load->helper('url');

//    $this->load->view('home_view');
    $user = $this->session->userdata('logged_in');
    if(isset($user)) {   // check if user is logged in or not...
      $data = array();
      $order = $this->session->userdata('order_data');
      $count = $this->orders_model->get_count('grocery_orders','*',array("order_status"=>'A'));

      if($order) {       // check if order data exists...
        $data = array('title'=>'HomeGrocery','user_data'=>$user,'order_data'=>$order,'order_count'=>sizeof($count));
      }
      else{
        $data = array('title'=>'HomeGrocery','user_data'=>$user);
      }
      $this->load->view('admin/admin_header',$data);
      $this->load->view('admin/admin_index',$data);
      $this->load->view('admin/admin_footer');

    }
    else{
      $this->load->view('admin/admin_login');
    }
  }

  function view($id) {
    $this->load->helper('url');
    $count = $this->orders_model->get_count('grocery_orders','*',array("order_status"=>'A'));

//    $this->load->view('home_view');
    $user = $this->session->userdata('logged_in');
    if(isset($user)) {
      $data = array();
      $order = $this->session->userdata('order_data');
      if($order) {

        $fields = array('det_product_id','det_product_name','det_price','det_quantity');
        $fields_data = $this->db_query->fields_string($fields);
        $where = array('det_order_id'=>$id);

        $this->orders_model->get_order_items('grocery_orders_detail',$fields_data,$where,function($status,$message,$data ) use($id,$user,$count){
          $items = array('status'=>$status,'message'=>$message,'data'=>$data);
          $order_detail = $this->orders_model->get_order_session($this->session->userdata('order_data'),$id);
          if($order_detail) {
            $data = array('title'=>'HomeGrocery','user_data'=>$user,'order_count'=>sizeof($count),
                'order_data'=>$order_detail,'order_items'=>$items);
          }
          else{
            $data = array('title'=>'HomeGrocery','user_data'=>$user,'order_count'=>sizeof($count));
          }

          $this->load->view('admin/admin_header',$data);
          $this->load->view('admin/admin_order',$data);
          $this->load->view('admin/admin_footer');

        });
//        print_r($order_items);
        /*if($order_items){
          $data = array('title'=>'HomeGrocery','user_data'=>$user,'order_count'=>sizeof($order),'order_data'=>$order,'order_items'=>$order_items);
        }
        else{
          $data = array('title'=>'HomeGrocery','user_data'=>$user);

        }*/
      }
      else{

        $data = array('title'=>'HomeGrocery','user_data'=>$user,'order_count'=>'0');
        $this->load->view('admin/admin_header',$data);
        $this->load->view('admin/admin_order',$data);
        $this->load->view('admin/admin_footer');

      }

    }
    else{
      $this->load->view('admin/admin_login');
    }
  }

  function state($type){
//    echo $type;
    $this->load->helper('url');

//    $this->load->view('home_view');
    $user = $this->session->userdata('logged_in');
    $count = $this->orders_model->get_count('grocery_orders','*',array("order_status"=>'A'));

    if(isset($user)) {   // check if user is logged in or not...
      $data = array();
      $order = $this->session->userdata('order_data');
      if($order){       // check if order data exists...
        $data = array('title'=>'HomeGrocery','user_data'=>$user,'order_data'=>$order,'order_type'=>$type,'order_count'=>sizeof($count));
      }
      else{
        $data = array('title'=>'HomeGrocery','user_data'=>$user,'order_count'=>'0');
      }
      $this->load->view('admin/admin_header',$data);
      $this->load->view('admin/admin_order_state',$data);
      $this->load->view('admin/admin_footer');

    }
    else{
      $this->load->view('admin/admin_login');
    }

  }

}

?>