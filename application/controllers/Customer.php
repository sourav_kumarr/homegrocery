<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 01-Oct-16
 * Time: 1:20 PM
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Controller{

    function __construct()
    {
     parent::__construct();
     $this->load->model('orders_model');
    }

     function index()
    {
        $this->load->helper('url');
        $user = $this->session->userdata('logged_in');
        if(isset($user)){
            $count = $this->orders_model->get_count('grocery_orders','*',array('order_status'=>'A'));
            $data = array('title'=>'HomeGrocery','order_count'=>sizeof($count),'user_data'=>$user);

            $this->load->view('admin/admin_header',$data);
            $this->load->view('admin/admin_users',$data);
            $this->load->view('admin/admin_footer',$data);

        }
         else{
//        $this->load->view('admin/admin_login');
          redirect('admin/', 'refresh');

         }
//    $this->load->view('home_view');
 }

}