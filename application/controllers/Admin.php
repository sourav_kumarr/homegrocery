<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

  function __construct()
  {
    parent::__construct();
  }

  function index()
  {
    $this->load->helper('url');
     $user = $this->session->userdata('logged_in');
     if(isset($user)){

       redirect('order/', 'refresh');
     }
    else{
      $this->load->view('admin/admin_login');
    }
//    $this->load->view('home_view');
  }

}

?>