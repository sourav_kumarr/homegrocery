<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {

  function __construct()
  {
    parent::__construct();
  }

  function index()
  {
    $this->load->helper('url');
     session_destroy();
      redirect('admin/', 'refresh');

//    $this->load->view('home_view');
  }

}

?>