<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 01-Oct-16
 * Time: 1:20 PM
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categoryy extends CI_Controller{

    function __construct()
    {
        parent::__construct();
        $this->load->model('orders_model');
        $this->load->model('category_model');

    }

     function index()
     {
        $this->load->helper('url');
        $user = $this->session->userdata('logged_in');
        if(isset($user)) {
            $count = $this->orders_model->get_count('grocery_orders','*',array('order_status'=>'A'));
             $fields = "cat_id, cat_name, cat_link, cat_parent_id, cat_sort_order";
             $where = array('cat_loc_id'=>'1');
             $this->category_model->get_category('grocery_category',$fields,$where,null,function($status,$message,$data) use($count,$user){
                 $results = array();
                 $category = array('categories' => array(), 'parent_cats' => array());
                 if($status) {
                     for( $i=0;$i<sizeof($data);$i++ ) {
                         $category['categories'][$data[$i]->cat_id] = $data[$i];
                         $category['parent_cats'][$data[$i]->cat_parent_id][] = $data[$i]->cat_id;
                     }
                     $results = array('status'=>$status,'message'=>$message,'data'=>$this->category_model->buildCategory(0, $category));

                 }
                 else{
                     $results = array('status'=>$status,'message'=>$message,'data'=>$data);
                 }
                 $cat_results = array('title'=>'HomeGrocery','order_count'=>sizeof($count),'user_data'=>$user,'cats'=>$results);
                 $this->load->view('admin/admin_header',$cat_results);
                 $this->load->view('admin/admin_category',$cat_results);
                 $this->load->view('admin/admin_footer',$cat_results);

             });


        }
         else {
//        $this->load->view('admin/admin_login');
          redirect('admin/', 'refresh');

         }
//    $this->load->view('home_view');
   }

}