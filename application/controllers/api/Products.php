<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Products extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('product_model','',TRUE);

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['items_get']['limit'] = 500; // 500 requests per hour per user/key
//        $this->methods['category_post']['limit'] = 100; // 100 requests per hour per user/key
//        $this->methods['category_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    /*
     * in this function all products will be fetched..
     * */

    function items_get()
    {
        $status = FALSE;
        $message = "unable to get products..";
        $cat_id = $this->input->get('cat_id');
        $result = $this->product_model->get_products($cat_id);

        if($result){
            $prod_data = array();
            $status = TRUE;
            $message = "data found..";
           $prod_data = $result;
            $this->set_response([
                'status' => $status,
                'message' => $message,
                'data'=>$prod_data
            ], REST_Controller::HTTP_OK); // HTTP_OK (200) being the HTTP response code

        }
        else{
            $status = FALSE;
            $message = "unable to get products from database..";
            $this->set_response([
                'status' => $status,
                'message' => $message
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

        }

    }

}
