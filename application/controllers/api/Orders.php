<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Orders extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('orders_model', '', TRUE);
        $this->load->library('validator');
        $this->load->library('db_query');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['items_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['items_post']['limit'] = 500; // 100 requests per hour per user/key
        $this->methods['items_delete']['limit'] = 500; // 100 requests per hour per user/key
        $this->methods['items_put']['limit'] = 500; // 100 requests per hour per user/key


//        $this->methods['category_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    /*
     * in this function all cart items will be fetched..
     * */

    function items_get()
    {
        $requiredfields = array('key','type');
//        print_r($this->get());
        $isValid = $this->validator->valid_params( $this->get() , $requiredfields);

//        echo $isValid;

        if ($isValid != "1") {
            $this->set_response([
                'status' => false,
                'message' => $isValid
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

        }
        else {
            $type=$this->get('type');
            $where_qr = '';
            if($type == 'details') {
                $requiredfields = array('id');
                $isValid = $this->validator->valid_params($this->get(), $requiredfields);

                if ($isValid != "1") {
                    $this->set_response([
                        'status' => false,
                        'message' => $isValid
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                    return ;
                }

                $id=$this->get('id');
                $where = array("order_user_id"=>$id);
                $where_qr = $this->db_query->where_string($where);
            }
            else if($type == 'all') {
                $where = array("order_status"=>'A');
                $where_qr = $this->db_query->where_string($where);
            }

            else if($type == "state") {
//                echo $type;
                $requiredfields = array('state');
                $isValid = $this->validator->valid_params($this->get(), $requiredfields);
//                $id=$this->get('id');

                if ($isValid != "1") {
                    $this->set_response([
                        'status' => false,
                        'message' => $isValid
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                   return ;
                }
                $state = $this->get('state');
                $where = array("order_state"=>$state,"order_status"=>'A');
                $where_qr = $this->db_query->where_string($where);
            }
//              echo $where_qr;
               $fields = array('order_id','order_date','order_state','order_payment_status','order_number',
                   'order_total','paid_amount','order_txn_id','order_currency_code','order_status','user_name','user_email');
              $fields_data = $this->db_query->fields_string($fields);
              $this->orders_model->get_order($where_qr,$fields_data,'grocery_orders',function($status,$message,$data=null)use($type){
                if($status){
                    $this->session->set_userdata('order_data', $data);

                    $this->set_response([
                        'status' => true,
                        'message' => $message,
                        'data' =>$data
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                }
                else{
                    $this->set_response([
                        'status' => false,
                        'message' => $message,
                        'data' =>$data
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                }
            });
        }
    }

    /*
     * in this function order will be posted..
     * */

     function items_post() {

         $requiredfields = array('order_state','order_payment_status','order_total',
                                'order_user_id','paid_amount','order_currency_code',
                                'order_txn_id','order_items');

         $isValid = $this->validator->valid_params( $this->input->post() , $requiredfields);
         if ($isValid != "1") {
             $this->set_response([
                 'status' => false,
                 'message' => $isValid
             ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

         }
         else{
             $order_state = $this->input->post('order_state');
             $order_payment_status = $this->input->post('order_payment_status');
             $order_number = "ORDER_".mt_rand(111111,999999);
             $order_total = $this->input->post('order_total');
             $order_user_id = $this->input->post('order_user_id');
             $paid_amount = $this->input->post('paid_amount');
             $order_currency_code = $this->input->post('order_currency_code');
             $order_txn_id = $this->input->post('order_txn_id');
             $order_items = json_decode($this->input->post('order_items'));
             if(sizeof($order_items)>0) {
                $data = array("order_state"=>$order_state,
                    "order_payment_status"=>$order_payment_status,
                    "order_number"=>$order_number,
                    "order_total"=>$order_total,
                    "order_user_id"=>$order_user_id,
                    "paid_amount"=>$paid_amount,
                    "order_currency_code"=>$order_currency_code,
                    "order_txn_id"=>$order_txn_id,
                    "order_status"=>'A');
               $this->orders_model->post_order($data,'grocery_orders',function($status,$message,$data) {
                   if($status) {
                         if($data>0) {
                             $order_items = json_decode($this->input->post('order_items'));
                             print_r($order_items);
                             $order_user_id = $this->input->post('order_user_id');

                             for($i=0;$i<sizeof($order_items) ;$i++) {

                                 $prod_id = $order_items[$i]->prod_id;
                                 $prod_name = $order_items[$i]->prod_name;
                                 $prod_price = $order_items[$i]->prod_price;
                                 $prod_quantity = $order_items[$i]->prod_quantity;

                                 $order_data = array("det_product_id"=>$prod_id,
                                                     "det_product_name"=>$prod_name,
                                                     "det_price"=>$prod_price,
                                                     "det_quantity"=> $prod_quantity,
                                                     "det_user_id"=>$order_user_id,
                                                     "det_order_id"=>$data,
                                                     "det_status"=>'A');
                                 $this->orders_model->post_order($order_data,'grocery_orders_detail',function($status,$message,$id) {
                                    if($status) {

                                        $this->set_response([
                                            'status' => true,
                                            'message' => $message
                                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                                    }
                                    else {

                                        $this->set_response([
                                            'status' => false,
                                            'message' => $message
                                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

                                    }
                                 });
                             }
                         }
                   }
                   else{
                       $this->set_response([
                           'status' => false,
                           'message' => $message
                       ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

                   }
               });
             }
         }
     }

     /*
      * in this function items will be deleted from cart
      *
      * */

      function items_delete() {
          $requiredfields = array('key','id');
//        print_r($this->get());
//          echo "hello";
          $isValid = $this->validator->valid_params( $this->delete() , $requiredfields);

//        print_r($this->delete());
//        echo $isValid;

          if ($isValid != "1") {
              $this->set_response([
                  'status' => false,
                  'message' => $isValid
              ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

          }
          else {

          }
      }

      /*
       * in this function data will be updated..
       *
       * */

       function items_put() {
           $requiredfields = array('key','id','items');
//           print_r($this->put());
           $isValid = $this->validator->valid_params( $this->put() , $requiredfields);

           if ($isValid != "1") {
               $this->set_response([
                   'status' => false,
                   'message' => $isValid
               ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

           }
           else {
               $id = $this->put('id');
               $items = json_decode($this->put('items'),true);

               $where = array('order_id'=>$id);
//               print_r($items);
               $this->orders_model->update_order_product('grocery_orders',$items[0],$where,function($status,$message){
                   if($status) {
                       $items = json_decode($this->put('items'),true);
                       $where_qr = array('order_state'=>'ship') ;
//                       print_r($where_qr);
                       $count = $this->orders_model->get_count('grocery_orders','*',$where_qr);
                       $this->set_response([
                           'status' => $status,
                           'message' => $message,
                           'data' =>$count
                       ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                   }
                   else{
                       $this->set_response([
                           'status' => $status,
                           'message' => $message
                       ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

                   }
               });

           }


       }






}
