<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Cart extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('cart_model', '', TRUE);
        $this->load->library('validator');
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['items_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['items_post']['limit'] = 500; // 100 requests per hour per user/key
        $this->methods['items_delete']['limit'] = 7; // 100 requests per hour per user/key
        $this->methods['items_put']['limit'] = 500; // 100 requests per hour per user/key


//        $this->methods['category_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    /*
     * in this function all cart items will be fetched..
     * */

    function items_get()
    {
        $requiredfields = array('key','id');
//        print_r($this->get());
        $isValid = $this->validator->valid_params( $this->get() , $requiredfields);

//        print_r($this->input->get());
//        echo $isValid;

        if ($isValid != "1") {
            $this->set_response([
                'status' => false,
                'message' => $isValid
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

        }
        else {
            $id = $this->get('id');
            $this->cart_model->get_cart_product($id, function ($status, $data) {
                if (!$status) {
                    $this->set_response([
                        'status' => false,
                        'message' => $data
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

                } else {
                    $this->set_response([
                        'status' => true,
                        'message' => $data
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                }
            });
        }
    }

    /*
     * in this function cart items will be posted..
     * */

     function items_post() {
         $requiredfields = array('prod_id','prod_name','prod_quantity','prod_image','prod_price','prod_total',
             'prod_subtotal','userid','key');
         $isValid = $this->validator->valid_params( $this->input->post() , $requiredfields);

         if ($isValid != "1") {
             $this->set_response([
                 'status' => false,
                 'message' => $isValid
             ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

         }
         else{
             $prod_id = $this->input->post('prod_id');
             $prod_name = $this->input->post('prod_name');
             $prod_quantity = $this->input->post('prod_quantity');
             $prod_image = $this->input->post('prod_image');
             $prod_price = $this->input->post('prod_price');
             $prod_total = $this->input->post('prod_total');
             $prod_subtotal = $this->input->post('prod_subtotal');
             $userid = $this->input->post('userid');
//             echo $this->cart_model->product_exist($userid,$prod_id);
             if(  !$this->cart_model->product_exist($userid,$prod_id)) {
                 $data = array("cart_prod_id"=>$prod_id,
                     "cart_prod_name"=>$prod_name,
                     "cart_prod_quantity"=>$prod_quantity,
                     "cart_prod_image"=>$prod_image,
                     "cart_prod_price"=>$prod_price,
                     "cart_prod_total"=>$prod_total,
                     "cart_prod_subtotal"=>$prod_subtotal,
                     "cart_userid"=>$userid);
                 $this->cart_model->post_cart_product($data,function($status,$message){
                     if($status){
                         $this->set_response([
                             'status' => true,
                             'message' => "item posted in cart"
                         ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                     }
                     else{
                         $this->set_response([
                             'status' => false,
                             'message' => $message
                         ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                     }
                 });
             }
             else{

                 $this->set_response([
                     'status' => false,
                     'message' => "product already exists in cart"
                 ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

             }
         }
     }

     /*
      * in this function items will be deleted from cart
      *
      * */

      function items_delete() {
          $requiredfields = array('key','id');
//        print_r($this->get());
//          echo "hello";
          $isValid = $this->validator->valid_params( $this->delete() , $requiredfields);

//        print_r($this->delete());
//        echo $isValid;

          if ($isValid != "1") {
              $this->set_response([
                  'status' => false,
                  'message' => $isValid
              ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

          }
          else {
              $id = $this->delete('id');
              $this->cart_model->delete_cart_product($id, function ($status, $message) {
                  if ($status) {
                      $this->set_response([
                          'status' => true,
                          'message' => $message
                      ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                  }
                  else {
                      $this->set_response([
                          'status' => false,
                          'message' => $message
                      ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

                  }
              });
          }
      }

      /*
       * in this function data will be updated..
       *
       * */

       function items_put() {
           $requiredfields = array('key','id','quantity');
//           print_r($this->put());
           $isValid = $this->validator->valid_params( $this->put() , $requiredfields);

           if ($isValid != "1") {
               $this->set_response([
                   'status' => false,
                   'message' => $isValid
               ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

           }
           else{
               $cart_id = $this->put('id');
               $quantity = $this->put('quantity');
               $data = array('cart_prod_quantity'=>$quantity);
               $this->cart_model->update_cart_product($cart_id,$data,function($status,$message){
                   if($status){
                       $this->set_response([
                           'status' => true,
                           'message' => $message
                       ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                   }
                   else{
                       $this->set_response([
                           'status' => false,
                           'message' => $message
                       ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

                   }
               });

           }


       }






}
