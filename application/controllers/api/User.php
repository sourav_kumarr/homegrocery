<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class User extends REST_Controller {

    public $otp =0;
    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->methods['users_put']['limit'] = 50; // 50 requests per hour per user/key

        $this->methods['login_put']['limit'] = 500; // 500 requests per hour per user/key

        //load user model models/User_model.php
        $this->load->model('user_model','',TRUE);

        //load curl and create

        $this->load->library('curl');
        $this->load->library('validator');
        $this->load->library('db_query');


        //  Setting URL To Fetch Data From

    }

    public function users_get()
    {
        // Users from a data store e.g. database

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            $requiredfields = array('key','type');
            $isValid = $this->validator->valid_params( $this->get() , $requiredfields);
            if ($isValid != "1") {
                $this->set_response([
                    'status' => false,
                    'message' => $isValid
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
            else{
                $type = $this->get('type');

                if($type == 'all') {

//                    $where = array("user_status" => '');
//                    $where_qr = $this->db_query->where_string( $where );
                    $fields = array('user_id','user_name', 'user_email', 'user_mobile','user_date','user_status');
                    $fields_data = $this->db_query->fields_string($fields);

                    $this->user_model->get_user_data('1', $fields_data, 'grocery_user', function ($status, $message, $data = null) {
                        if ($status) {
                            $this->set_response([
                                'status' => true,
                                'message' => $message,
                                'data' => $data
                            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                        } else {
                            $this->set_response([
                                'status' => FALSE,
                                'message' => $message,
                                'data' => $data
                            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

                        }
                    });
                }
                else if($type == 'login') {
                    $requiredfields = array('email','password');
                    $isValid = $this->validator->valid_params( $this->get() , $requiredfields);
                    if ($isValid != "1") {
                        $this->set_response([
                            'status' => false,
                            'message' => $isValid
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                    }
                    else {


                        $email = $this->get('email');
                        $password = md5($this->get('password'));
                        if ($this->validator->isValidEmail($email)) {

                            $where = array("user_email" => $email, "user_password" => $password);
                            $where_qr = $this->db_query->where_string($where, "and");
                            $fields = array('user_name', 'user_email', 'user_mobile');
                            $fields_data = $this->db_query->fields_string($fields);
                            $this->user_model->get_user_data($where_qr, $fields_data, 'grocery_user', function ($status, $message, $data = null) {
                                if ($status) {
                                    $this->set_response([
                                        'status' => true,
                                        'message' => $message,
                                        'data' => $data
                                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                                    $this->session->set_userdata('logged_in', $data);

                                } else {
                                    $this->set_response([
                                        'status' => FALSE,
                                        'message' => $message,
                                        'data' => $data
                                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

                                }
                            });
                        } else {
                            $this->set_response([
                                'status' => FALSE,
                                'message' => "invalid email-id found"
                            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

                        }
                    }
                }

            }


        }

        // Find and return a single record for a particular user.
    }

    /*
     * create new user
     *
     * */

    public function users_post()
    {
        // $this->some_model->update_user( ... );
        $type = $this->post('user_type');
//        echo $type;
//        print_r($this->post);

        if($type == 'request_token') {
            $user_name = $this->post('user_name');
            $user_email = $this->post('user_email');
            $user_mobile = $this->post('user_mobile');
            $user_password = $this->post('user_password');

            if(empty($user_name)){
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'user name shuould not be empty'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

            }
            else if(empty($user_email)){
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'user email shuould not be empty'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

            }
            else if(empty($user_password)){
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'user password shuould not be empty'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

            }
            else if(empty($user_mobile)){
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'user mobile shuould not be empty'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

            }
            else if(!$this->user_model->validate_mobile_number($user_mobile)){
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'please enter valid mobile number'
                ], REST_Controller::HTTP_NOT_FOUND);
            }
            else if($this->user_model->validate_email($user_mobile)){
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'please enter valid email-id'
                ], REST_Controller::HTTP_NOT_FOUND);

            }
            else{
                $otp = mt_rand(111111,999999);
                $data = array('user_name'=>$this->post('user_name'),
                    'user_email'=>$user_email,
                    'user_password'=>md5($user_password),
                    'user_mobile'=>$user_mobile,
                    'user_status'=>'D',
                     'user_otp'=>$otp);
                if(!$this->user_model->isUserExists($user_mobile,$user_email)) {


                    $result = $this->user_model->create_user($data);
                    if ($result) {
                        $url = "http://www.kit19.com/ComposeSMS.aspx?username=stsoln646272&password=19963&sender=STSOLN&to=".$user_mobile."&message=".$otp."&priority=1&dnd=1&unicode=0";

                        $data =  $this->curl->simple_get($url);

                        sleep(10);
//                         echo $data;
                        if($data == "Sent.") {
                            $this->set_response([
                                'status' => true,
                                'message' => 'User created successfully',
                                'otp' =>$otp,
                                'otp_status'=>$data,
                                'user_data' =>$result
                            ], REST_Controller::HTTP_OK);

                        }
                        else{
                            $this->set_response([
                                'status' => true,
                                'message' => 'User created successfully',
                                'otp' =>$otp,
                                'otp_status'=>$data,
                                'user_data' =>$result
                            ], REST_Controller::HTTP_OK);

                        }
                    }
                    else{
                        $this->set_response([
                            'status' => false,
                            'message' => 'unable to create user',
                        ], REST_Controller::HTTP_NOT_FOUND);

                    }
                }
                else{
                    $this->set_response([
                        'status' => false,
                        'message' => 'user already exists',
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            }


        }
        else if($type == 'activate_account'){

             $user_id = $this->post('user_id');
             $otp = $this->post('otp');
             $error = '';
            $isOtp =  $this->user_model->validate_otp($otp,$user_id);
//            echo $isOtp;
            $result = 0;
            if($isOtp) {
                $data = array('user_status'=>'A');
                $result = $this->user_model->update_user($user_id,$data);
                if(!$result){
                    $error ='unable to update data';
                }
            }
            else{
               $error = 'invalid otp send';
            }
            if(strlen($error)>0){
                $this->set_response([
                    'status' => false,
                    'message' => $error,
                ], REST_Controller::HTTP_NOT_FOUND);
            }
            else{
                $this->set_response([
                    'status' => true,
                    'message' => 'user created successfully',
                    'user_data'=>$result
                ], REST_Controller::HTTP_OK);

            }

        }

        else{
//            echo $type;
            $this->set_response([
                'status' => false,
                'message' => 'Invalid request type',
            ], REST_Controller::HTTP_NOT_FOUND);

        }
//        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    /*
     * in this function user data will be updated...
     * */

    public function users_put() {

        $id = (int) $this->put('id');
        if(!($id === null)) {
            $requiredfields = array('items');
            $isValid = $this->validator->valid_params($this->put(), $requiredfields);
//                $id=$this->get('id');

            if ($isValid != "1") {
                $this->set_response([
                    'status' => false,
                    'message' => $isValid
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                return ;
            }
            else{
                $items = json_decode($this->put('items'),true);

                $where = array('user_id'=>$id);
                $this->user_model->update_user_data('grocery_user',$items[0],$where,function($state,$message,$data=null){
                    if($state){
                        $this->set_response([
                            'status' => true,
                            'message' => $message
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                    }
                    else{
                        $this->set_response([
                            'status' => false,
                            'message' => $message,
                            'data' => $data
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

                    }
                });
            }
        }
    }

    public function users_delete()
    {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = [
            'id' => $id,
            'message' => 'Deleted the resource'
        ];

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }

    public function login_get() {

        $mobile=$this->get('mobile');
        $password=$this->get('password');
        if(empty($mobile)) {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Mobile number should not be empty'
            ], REST_Controller::HTTP_NOT_FOUND);

        }
        else if(empty($password)){
            $this->set_response([
                'status' => FALSE,
                'message' => 'password should not be empty'
            ], REST_Controller::HTTP_NOT_FOUND);

        }
        else if(!$this->user_model->validate_mobile_number($mobile)){
            $this->set_response([
                'status' => FALSE,
                'message' => 'please enter valid mobile number'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
        else {
           // user logged in successfully..
            $result = $this->user_model->login($mobile,$password);

            $otp = mt_rand(111111,999999);

             if($result){

                 $url = "http://www.kit19.com/ComposeSMS.aspx?username=stsoln646272&password=19963&sender=STSOLN&to=".$mobile."&message=".$otp."&priority=1&dnd=1&unicode=0";

                 $data =  $this->curl->simple_get($url);

                 sleep(10);
//                 echo $data;
                if($data == "Sent."){
                    $this->set_response([
                        'status' => true,
                        'message' => 'User logged in  successfully',
                        'otp' =>$otp,
                        'otp_status'=>'send',
                        'user_data' =>$result
                    ], REST_Controller::HTTP_OK);

                }
                else{
                    $this->set_response([
                        'status' => true,
                        'message' => 'User logged in  successfully',
                        'otp' =>$otp,
                        'otp_status'=>'not send',
                        'user_data' =>$result
                    ], REST_Controller::HTTP_OK);

                }
            }
            else{
                $this->set_response([
                    'status' => false,
                    'message' => 'User entered invalid credentials'
                ], REST_Controller::HTTP_NOT_FOUND);

            }

        }

    }

}
