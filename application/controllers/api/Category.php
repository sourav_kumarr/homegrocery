<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Category extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('category_model','',TRUE);
        $this->load->library('validator');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['items_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['items_post']['limit'] = 100; // 100 requests per hour per user/key
//        $this->methods['category_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    /*
     * in this function all category will be fetched..
     * */

    function items_get()
    {
        $loc_id = $this->input->get('loc_id');
        $fields = "cat_id, cat_name, cat_link, cat_parent_id, cat_sort_order";
        $where = array('cat_loc_id'=>$loc_id);

        $this->category_model->get_category('grocery_category',$fields,$where,null,function($status,$message,$data){
            if($status) {
                $flat = array();
                $cat_data = array();
                foreach($data as $row)
                {
                    $flat[] = array("id"=>$row->cat_id,"parentID"=>$row->cat_parent_id,"name"=>$row->cat_name);

                }
                $cat_data = $this->category_model->buildTree($flat, 'parentID', 'id');
                $status = TRUE;
                $this->set_response([
                    'status' => $status,
                    'message' => $message,
                    'data'=>$cat_data
                ], REST_Controller::HTTP_OK); // HTTP_OK (200) being the HTTP response code

            }
            else{
                $status = FALSE;
                $message = "unable to get data from database..";
                $this->set_response([
                    'status' => $status,
                    'message' => $message,
                    'data' =>$data
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

            }
        });



    }

    /*
     * in this function category will be posted
     *
     * */

    function items_post() {

        $requiredfields = array('key','cat_id','sort_id','parent_id','cat_name','loc_id');
//        print_r($this->get());
        $isValid = $this->validator->valid_params( $this->post() , $requiredfields);

//        print_r($this->input->get());
//        echo $isValid;

        if ($isValid != "1") {
            $this->set_response([
                'status' => false,
                'message' => $isValid
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

        }
        else{
            $cat_id = $this->post('cat_id');
            $cat_name = $this->post('cat_name');

            $fields = '*';
            $where = array('cat_id'=>$cat_id);
            $where_not = array('cat_link'=>'#','cat_name'=>'Home');
            $this->category_model->get_category('grocery_category',$fields,$where,$where_not,function($status,$message,$data) use($cat_id,$cat_name) {
              if($status) {
                 $count = sizeof($data);

                  if($count>0) {
                    $data = array('cat_link'=>'#');
                    $where = array('cat_id'=>$cat_id);

                    $this->category_model->update_category('grocery_category',$data,$where,function($status,$message,$data) use($cat_id,$cat_name){
                        if($status) {

                            $sort_id = $this->post('sort_id');
                            $parent_id = $this->post('parent_id');
                            $loc_id = $this->post('loc_id');
                            $link_id = strtolower($cat_name);
                            $data = array('cat_name'=>$cat_name,'cat_link'=>$link_id,'cat_parent_id'=>$parent_id,'cat_sort_order'=>$sort_id,'cat_loc_id'=>$loc_id);
                            /*
                             * inseerrt cat data here.....
                             * */
                            $this->category_model->post_category('grocery_category',$data,function($status,$message,$data){
                                if($status) {

                                    $fields = "cat_id, cat_name, cat_link, cat_parent_id, cat_sort_order";
                                    $where = array('cat_loc_id'=>'1');

                                    $this->category_model->get_category('grocery_category',$fields,$where,null,function($status,$message,$data){
                                        if($status){
                                            $category = array('categories' => array(), 'parent_cats' => array());

                                            for( $i=0;$i<sizeof($data);$i++ ) {
                                                $category['categories'][$data[$i]->cat_id] = $data[$i];
                                                $category['parent_cats'][$data[$i]->cat_parent_id][] = $data[$i]->cat_id;
                                            }
                                            $this->set_response([
                                                'status' => true,
                                                'message' => $message,
                                                'data' => $this->category_model->buildCategory(0, $category)
                                            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                                        }
                                        else{
                                            $this->set_response([
                                                'status' => false,
                                                'message' => $message,
                                                'data' => $data
                                            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                                        }

                                    });

                                }
                                else{
                                    $this->set_response([
                                        'status' => false,
                                        'message' => $message,
                                        'data' => $data
                                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

                                }
                              });
                        }
                        else{  // update category error here -------
                            $this->set_response([
                                'status' => false,
                                'message' => $message,
                                'data' => $data
                            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

                        }
                    });
                  }
                  else{
                      $sort_id = $this->post('sort_id');
                      $parent_id = $this->post('parent_id');
                      $loc_id = $this->post('loc_id');
                      $link_id = strtolower($cat_name);
                      $data = array('cat_name'=>$cat_name,'cat_link'=>$link_id,'cat_parent_id'=>$parent_id,'cat_sort_order'=>$sort_id,'cat_loc_id'=>$loc_id);
                      /*
                       * inseerrt cat data here.....
                       * */
                      $this->category_model->post_category('grocery_category',$data,function($status,$message,$data){
                          if($status){
                              $this->set_response([
                                  'status' => false,
                                  'message' => $message,
                                  'data' => $data
                              ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                          }
                          else{
                              $this->set_response([
                                  'status' => false,
                                  'message' => $message,
                                  'data' => $data
                              ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

                          }
                      });
                  }
              }
              else{ // get category error here--------
                  $this->set_response([
                      'status' => false,
                      'message' => $message,
                      'data' =>$data
                  ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

              }
           });
        }
    }
}
