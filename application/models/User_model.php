<?php
Class User_model extends CI_Model
{
	function login($mobile, $password)
	{
		$this -> db -> select('user_id, user_name, user_mobile,user_email');
		$this -> db -> from('grocery_user');
		$this -> db -> where('user_mobile = ' . "'" . $mobile . "'");
		$this -> db -> where('user_password = ' . "'" . MD5($password) . "'");
		$this -> db -> where('user_status = A');

		$this -> db -> limit(1);

		$query = $this -> db -> get();

		if($query -> num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}

	}

	    /*
         * in this function user detail will be fetched
         * */

	function get_user_data($where,$fields,$table,$callback) {
		$this -> db -> select($fields);
		$this -> db -> from($table);
		if($where != '1'){
			$this -> db -> where($where);

		}

//		$this -> db -> limit(1);
		$query = $this -> db -> get();

		if(!$query){
			return $callback(false,"unable to get data from $table",$this->db->error()['message']);

		}
		else{
			 if($query -> num_rows() >= 1) {
				 $result = 	$query->result();
				 return $callback(true,"got data",$result);

			 }
			else{
				return $callback(false,"user not found");

			}
        }

	}



	/*
	 * create new user
	 *
	 * */

     function  create_user($data) {
		 $str = $this-> db->insert_string('grocery_user', $data);
		 $this -> db -> query($str);
		 $id = $this->db->insert_id();

		 if($id > 0){
			 return $id;
		 }
		 else{
			 return false;
		 }
	 }

     /*
      * in this function user will be validate user exists or not
      * */

	  function isUserExists($mobile,$email) {
		  $this -> db -> select('*');
		  $this -> db -> from('grocery_user');
		  $this -> db -> where('user_mobile = ' . "'" . $mobile . "'");
		  $this -> db -> where('user_email = ' . "'" . $email . "'");

		  $query = $this -> db -> get();

		  if($query -> num_rows() >0 )
		  {
			  return true;
		  }
		  else
		  {
			  return false;
		  }
	  }
      /*
       * in this function user data will be updated
       * */

	  function update_user($id,$data){

		  $this->db->where('user_id', $id);
		  $this->db->update('grocery_user', $data);
		  $rows = $this->db->affected_rows();
          if($rows == 1){
			  return $this->get_user_data($id);
		  }
		  return false;


	  }

	/*
	 *in this function otp will be validated
	 * */

	function validate_otp($otp,$user_id){
		$this -> db -> select('*');
		$this -> db -> from('grocery_user');
		$this -> db -> where('user_id = ' . "'" . $user_id . "'");
		$this -> db -> where('user_otp = ' . "'" . $otp . "'");

		$query = $this -> db -> get();

		if($query -> num_rows() >0 )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/*
	 *validate mobile number...
	 * */

	function validate_mobile_number($number){
//        echo preg_match("/^[0-9]{10}*$/",$number)."<br/>";
//		echo strlen($number);

		if(!preg_match('/^0\d{10}$/',$number) && strlen($number)!= 10)
		{
		  return false;
        }
		return true;
	}

	/*
	 * validate email id..
	 * */

	function validate_email($email){
		$reg = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';

		if (preg_match($reg, $email)) {
			return true;
           }
		return false;
	  }

	/*
        * in this function user will be updated will be updated
        * */

	function update_user_data($table,$data,$where,$callback) {

		$result = $this->db->update($table,$data,$where);
		if(!$result) {
			$err = $this->db->error()['message'];
			$callback(false,$err);
		}
		else{

			$callback(true,"data updated successfully");
		}

	}
}
?>