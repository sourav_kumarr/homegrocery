<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 13-Sep-16
 * Time: 3:52 PM
 */
Class Category_model extends CI_Model
{
    function get_category($table,$fields,$where,$where_not = null,$callback) {
        $this ->db ->select($fields);
        $this -> db -> from($table);
        $this->db->where($where);
        if($where_not!=null) {
            $this->db->where_not_in($where_not);
        }
        $query = $this -> db -> get();

        if($query )
        {
            $result = $query->result();
            return $callback(true,"Category found",$result);
        }
        else
        {
            $err = $this->db->error()['message'];

            return $callback(false,"No record found yet",$err);
        }

    }

    /*
     * in this function category will be posted..
     *
     * */

    function post_category($table,$data,$callback) {
        $str = $this->db->insert_string($table, $data);
        $query = $this->db->query($str);
        if(!$query) {
//               echo $this->db->_error_message();
//               echo  $this->db->error()['message'];
            return $callback(false,"insertion error in $table",$this->db->error()['message']);
        }
        else {
            $id = $this->db->insert_id();
            if ($id > 0) {
                return $callback(true,"data uploaded",$id);
            } else {
                return $callback(false,"insertion error in $table",$id);
            }

        }
    }

    /*
     * in this function data will be updated..
     *
     * */

      function update_category ($table,$data,$where,$callback) {
          $result = $this->db->update($table,$data,$where);
          if(!$result) {
              $err = $this->db->error()['message'];
              $callback(false,$err);
          }
          else{

              $callback(true,"data updated successfully",null);
          }

      }


    /*
     * in this function flat array will be converted into tree array
     * */


    function buildTree($flat, $pidKey, $idKey = null)
    {
        $grouped = array();
        foreach ($flat as $sub){
            $grouped[$sub[$pidKey]][] = $sub;
        }

        $fnBuilder = function($siblings) use (&$fnBuilder, $grouped, $idKey) {
            foreach ($siblings as $k => $sibling) {
                $id = $sibling[$idKey];
                if(isset($grouped[$id])) {
                    $sibling['subcategory'] = $fnBuilder($grouped[$id]);
                }
                $siblings[$k] = $sibling;
            }

            return $siblings;
        };

        $tree = $fnBuilder($grouped[0]);

        return $tree;
    }

    /*
     * in this function category buildCategory will returns cat html data
     *
     * */

    function buildCategory($parent, $category) {
        $html = "";
//      echo "buildCategory is called";
        if (isset($category['parent_cats'][$parent])) {
            $html .= " <ul class='cat-style' id='".$parent."'>\n";

            foreach ($category['parent_cats'][$parent] as $cat_id) {
                if (!isset($category['parent_cats'][$cat_id])) {
                    /*
                     * this will display children items..
                     * */

                    $cat_name = $category['categories'][$cat_id]->cat_name;
                    $cat_name = urldecode($cat_name);
                    $html .= "<li style='cursor: pointer;padding-top:2%'>  <label>" . $category['categories'][$cat_id]->cat_name .
                    "</label><button class='btn btn-default btn-sm' style='position:absolute;right:0px;margin-right:179px' onclick=add_category('Add','".$category['categories'][$cat_id]->cat_id."','".$category['categories'][$cat_id]->cat_sort_order."')> <i class='fa fa-plus'></i> Add</button>".
                        "<button class='btn btn-default btn-sm' style='position:absolute;right:0px;margin-right:119px' onclick=edit_category('".$cat_name."','".$category['categories'][$cat_id]->cat_id."','".$category['categories'][$cat_id]->cat_sort_order."')><i class='fa fa-pencil'></i> Edit</button>".
                        "<button class='btn btn-default btn-sm' style='position:absolute;right:0px;margin-right:46px' onclick=show_message('Add','".$category['categories'][$cat_id]->cat_id."','".$category['categories'][$cat_id]->cat_sort_order."')> <i class='fa fa-remove'></i> Delete</button>".
                        " </li> ";

                }
                if (isset($category['parent_cats'][$cat_id])) {

                    /*
                     * this will display parent items..
                     * */
                    $cat_name = $category['categories'][$cat_id]->cat_name;
                    $cat_name = urlencode($cat_name);
                    $html .= " <li style='cursor: pointer;padding-top:2%' > <i class='fa fa-plus'  id=i_".$category['categories'][$cat_id]->cat_id." onclick=item_clicked('".$category['categories'][$cat_id]->cat_id."')></i> <label>" . $category['categories'][$cat_id]->cat_name .
                        "</label><button class='btn btn-default btn-sm' style='position:absolute;right:0px;margin-right:179px' onclick=add_category('Add','".$category['categories'][$cat_id]->cat_id."','".$category['categories'][$cat_id]->cat_sort_order."')> <i class='fa fa-plus'></i> Add</button>".
                        "<button class='btn btn-default btn-sm' style='position:absolute;right:0px;margin-right:119px' onclick=edit_category('".$cat_name."','".$category['categories'][$cat_id]->cat_id."','".$category['categories'][$cat_id]->cat_sort_order."')><i class='fa fa-pencil'></i> Edit</button>".
                        "<button class='btn btn-default btn-sm' style='position:absolute;right:0px;margin-right:46px' onclick=show_message('Add','".$category['categories'][$cat_id]->cat_id."','".$category['categories'][$cat_id]->cat_sort_order."')><i class='fa fa-remove'></i> Delete</button>";
                    $html .= $this->buildCategory($cat_id, $category);
                    $html .= "</li> ";
//                echo $category['categories'][$cat_id]['cat_name']."  ";
                    $json_items[] = array("cat_id"=>$cat_id,"cat_name"=>$category['categories'][$cat_id]->cat_name);

                }
            }
            $html .= "</ul> \n";
        }
        return $html;
    }



}
?>