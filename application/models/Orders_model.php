<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 13-Sep-16
 * Time: 3:52 PM
 */
Class Orders_model extends CI_Model
{


    /*
     * in this function
     * */
    function get_order($where,$fields,$table,$callback){
//        echo $where;
        $this -> db -> select($fields);
        $this -> db -> from($table);
        $this-> db ->join('grocery_user',$table.'.order_user_id = grocery_user.user_id');
        $this -> db -> where($where);
        $query = $this -> db -> get();
//        echo $query -> num_rows();
        if(!$query){
            return $callback(false,"unable to get data from $table",$this->db->error()['message']);

        }
        else{
            if($query -> num_rows() > 0)
            {
                $result = $query->result();
                return $callback(true,"got data",$result);
            }
            else
            {
                return $callback(false,"No records found");
            }
        }


    }

   /*
    * in this function product will posted to order table
    *
    * */

   function post_order($data,$table,$callback) {

           $str = $this->db->insert_string($table, $data);
           $query = $this->db->query($str);
           if(!$query) {
//               echo $this->db->_error_message();
//               echo  $this->db->error()['message'];
               return $callback(false,"insertion error in $table",$this->db->error()['message']);
           }
           else {
               $id = $this->db->insert_id();
               if ($id > 0) {
                   return $callback(true,"data uploaded",$id);
               } else {
                   return $callback(false,"insertion error in $table",$id);
               }

           }

   }

     /*
      * get order items..
      * */

     function get_order_items($table,$fields,$where,$callback) {
         $this -> db -> select($fields);
         $this -> db -> from($table);
         $this -> db -> where($where);
         $query = $this -> db -> get();
//        echo $query -> num_rows();
         if(!$query){
             return $callback(false,"unable to get data from $table",$this->db->error()['message']);
         }
         else{
             if($query -> num_rows() > 0)
             {
                 $result = $query->result();
                 return $callback(true,"got data",$result);
//                 return $result;
             }
             else
             {
                 return $callback(false,"No records found");
//                 return false;
             }
         }

     }

    /*
     * in this function cart items will be updated
     * */

    function update_order_product($table,$data,$where,$callback) {

        $result = $this->db->update($table,$data,$where);
        if(!$result) {
            $err = $this->db->error()['message'];
            $callback(false,$err);
        }
        else{

            $callback(true,"data updated successfully");
        }

    }


    /*
     * get total items..
     * */
     function get_count($table,$fields,$where) {
         $this -> db -> select($fields);
         $this -> db -> from($table);
         $this -> db -> where($where);
         $query = $this -> db -> get();
//       else{
         if($query) {
             if ($query->num_rows() > 0) {
                 return $query->result();
             } else {
                 return false;
             }
         }
        return false;
     }

    /*
     * get order from sessiion
     * */

    function get_order_session($orders,$id) {
        for($i=0;sizeof($orders);$i++){
            $order_id = $orders[$i]->order_id;
            if($order_id == $id){
                return $orders[$i];
            }
        }
        return false;
    }
}


?>