<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 13-Sep-16
 * Time: 3:52 PM
 */
Class Cart_model extends CI_Model
{

    /*
     * in this function product will be check in cart
     * */

    function product_exist($user_id,$prod_id)
    {
        $this -> db -> select('*');
        $this -> db -> from('grocery_cart');
        $this -> db -> where('cart_userid = ' . "'" . $user_id. "' and cart_prod_id='".$prod_id."'");
        $query = $this -> db -> get();
//        echo $query -> num_rows();
        if($query -> num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }

    }

   /*
    * in this function product will posted to poduct cart
    *
    * */

   function post_cart_product($data,$callback) {

           $str = $this->db->insert_string('grocery_cart', $data);
           $query = $this->db->query($str);
           if(!$query) {
//               echo $this->db->_error_message();
//               echo  $this->db->error()['message'];
               return $callback(false,$this->db->error()['message']);
           }
           else{
               $id = $this->db->insert_id();
               if ($id > 0) {
                   return $callback(true,$id);
               } else {
                   return $callback(false,"invalid id found");
               }

           }

   }

   /*
    * in this function products will be displayed that is in cart
    *
    * */

    function get_cart_product($id,$callback) {

            $this -> db -> select('*');
            $this -> db -> from('grocery_cart');
            $this -> db -> where('cart_userid = ' . "'" . $id . "'");

            $query = $this -> db -> get();
            if(!$query) {
                $err = $this->db->error()['message'];
                $callback(false,$err);
            }
            else{
                if($query -> num_rows() >0 )
                {
                    $callback(true,$query->result());
                }
                else
                {
                    $callback(true,"No items found in cart");
                }
            }


    }

    /*
     * in this function products in cart will be removed
     *
     * */

    function delete_cart_product($id,$callback) {
        $count = $this->db->count_all('grocery_cart');
        $this->db->where('cart_userid', $id);
        $this->db->delete('grocery_cart');
        $new_count = $this->db->count_all('grocery_cart');
//        echo "new ".$new_count." old ".$count;
        if($count > 0) {

            if ($new_count < $count) {
                $callback(true, "row deleted");
            } else {
                $callback(false, "unable to delete record");

            }
        }
        else{
            $callback(false, "cart is already empty");
        }
    }

    /*
     * in this function cart items will be updated
     * */

    function update_cart_product($cart_id,$data,$callback){
        $this->db->where('cart_id', $cart_id);

        $result = $this->db->update('grocery_cart', $data);
        if(!$result) {
            $err = $this->db->error()['message'];
            $callback(false,$err);
        }
        else{
            $callback(true,"data updated successfully");
        }

    }


}
?>