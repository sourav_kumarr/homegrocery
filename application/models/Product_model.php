<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 14-Sep-16
 * Time: 12:46 PM
 */
Class Product_model extends CI_Model
{
    function get_products($cat_id)
    {
        $this->db->select('*');
        $this->db->from('grocery_products');
        $this->db->join('grocery_products_images', 'grocery_products_images.image_prod_id= grocery_products.prod_id','left');
        $this->db->where('grocery_products.prod_cat_id', $cat_id);
        $query = $this->db->get();
//        echo $query -> num_rows();
        if($query -> num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }

    }


}
?>