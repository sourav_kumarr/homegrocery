<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 26-Sep-16
 * Time: 11:40 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

 class Db_query extends CI_Model{

     public function __construct()
     {
         //validator constructor is called
         parent::__construct();


     }

     /*
      * make where query here
      * */
      public function where_string($params,$condition=null) {
          $str = '';
          if(sizeof($params)>0){
              $str = "";
              $counter = 0;
              foreach($params as $key=>$val)
              {
                if($condition!=null){
                    if(sizeof($params)-1 == $counter){
                        $str = $str." ".$key."='".$val."'";
                    }
                    else{
                        $str = $str." ".$key."='".$val."' $condition ";

                    }
                }
                else{
                    $str = $str." ".$key."='".$val."'";
                    break;
                }
                $counter++;
              }
          }
          return $str;
      }

     /*
      * in this function fields string will be make
      * */
     public function fields_string( $fields ) {
         $str = '';
         if(sizeof($fields)>0) {

           for($i=0;$i<sizeof($fields);$i++){
             if($i == sizeof($fields)-1){
                 $str = $str.$fields[$i];
             }
             else{
                 $str = $str.$fields[$i].",";
             }
           }
         }
         return $str;
     }
 }
?>