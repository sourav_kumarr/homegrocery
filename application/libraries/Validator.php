<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 26-Sep-16
 * Time: 11:40 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

 class Validator extends CI_Model{

     public function __construct($config = 'validate_message')
     {
         //validator constructor is called
         parent::__construct();

         $this->load->config($config);

     }

     /*
      * check for valid email-id
      * */
     public function isValidEmail($email) {
         $reg = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';

         if (preg_match($reg, $email)) {
             return true;
         }
         return false;

     }

     /*
      * check  for valid mobile number..
      *
      * */

     public function validate_mobile_number($number) {
//        echo preg_match("/^[0-9]{10}*$/",$number)."<br/>";
//		echo strlen($number);

         if(!preg_match('/^0\d{10}$/',$number) && strlen($number)!= 10)
         {
             return false;
         }
         return true;
     }

     /*
      * in this function params will be validated with post or get params
      * */

     public function valid_params($params,$requiredfields) {
         $echoErr = true;
         if(count($params)< count($requiredfields)) {

             $error = implode(",",$requiredfields);
             if($echoErr) {
                return $this->config->item('message').$error;
             }
             //die();
//             return 0;
         }
         foreach ($requiredfields as $key) {
             if(array_key_exists($key, $params)) {
                 if (!isset($params[$key])) {
                     $error = implode(",",$requiredfields);
                 }
             }
             else{
                 $error = implode(",",$requiredfields);
             }
         }
         if(isset($error) && $echoErr) {
//             errorMessage(errorCode::$generic_param_missing.$error,errorCode::$generic_param_missing_code);
             //die();
             return $this->config->item('message').$error;
//             return 0;
         }
         return "1";
     }
 }
?>