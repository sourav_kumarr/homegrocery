var random = Math.floor(Math.random()*1000000);
var sch_id = "Guest"+random;
var parent_id = 0;
var level_id = 0
var folder_path = '';

if($("#sch_id").val() != undefined){
    sch_id = $("#sch_id").val();
}
window.onload = function () {
    console.log('load function is callled');
    $('#datatable-buttons').on('draw.dt', function() {
        $('.videoStatus').bootstrapToggle();
    });
};

function onTimeChange() {
  $("#priority").val('na');
}


////////////////////////////////////// folder module here  ////////////////////////////////////

function onLevelLoaded(step,id,path) {
    console.log('step -- '+step);
    $(".folders").removeClass('active');
    $(".folders.folders-"+step).addClass('active');
    parent_id = id;
    folder_path = decodeURIComponent(path).replace(/\++/g, ' ')+'/';
    level_id = step;
}

function loadFolderData() {
 console.log('loadFolderData--- '+parent_id);
 var url = 'api/folderProcess.php';
 $.post(url,{parent_id:parent_id,type:'getFolderData'},function(data){
     console.log(JSON.stringify(data));
     var status = data.Status;
     var message = data.Message;

     if(status == 'Success') {
       var data_ = '<thead><tr><th>#</th><th>Name</th><th>Path</th><th>Created At</th><th>Action</th></tr></thead><tbody>';
       var folders = data.folders;
       var videos = data.videos;
       var counter = 1;
       // load folders data
         var path_ = '';
       for(var i=0;i<folders.length;i++) {
            path_ = folders[i].folder_path;
           path_ = path_.substr(path_.search('video'),path_.length);
           data_ = data_+'<tr><td>'+counter+'</td><td ><span style="cursor:pointer" onclick=onLevelLoaded("3","'+folders[i].folder_id+'","'+encodeURI(folders[i].folder_path)+'");loadFolderData();><i class="fa fa-folder"></i> '+folders[i].folder_name+'</span></td><td>'+path_+'</td>' +
               '<td>'+folders[i].folder_created_at+'</td><td> ' +
               '<i class="fa fa-trash" style="color:#D05E61;cursor: pointer" onclick=deleteFolder("'+folders[i].folder_id+'","'+encodeURI(folders[i].folder_path)+'")></i></td></tr>';
           counter++;
       }

       // load videos data

         for(var i=0;i<videos.length;i++) {
             var video_name = videos[i].video_name.split(";")[1].split(".mp4")[0];
             var date = toHumanFormat(videos[i].added_on);
             path_ = videos[i].video_path;
             path_ = path_.substr(path_.search('video'),path_.length);

             data_ = data_+'<tr><td>'+counter+'</td><td><i class="fa fa-video-camera"></i> '+video_name+'</td><td>'+path_+
                 '<td>'+date+'</td><td><i class="fa fa-edit" style="color:#D05E61;cursor: pointer;" onclick=editVideo("'+videos[i].video_id+'","'+encodeURI(videos[i].video_name)+'")></i> ' +
                 '<i class="fa fa-trash" style="color:#D05E61;cursor: pointer" onclick=deleteVideo("'+videos[i].video_id+'")></i></td></tr>';
             counter++;
         }
         if(level_id == 2)
         $("#datatable-buttons1").html(data_);
         else if(level_id == 3)
          $("#datatable-buttons2").html(data_);

     }
     else if(status == 'Error') {
         if(level_id == 2)
             $("#datatable-buttons1").html(message);
         else if(level_id == 3)
             $("#datatable-buttons2").html(message);
     }
 });

}
function addFolder() {
    $('.modal-title').html('<label>Add Folder</label>');
    $('.modal-body').html('<div class="col-md-12"><input type="text" id="folder_name_" placeholder="Please enter folder name" class="form-control" /></div>')
    $('.modal-body').css('height','70px');
    $('.modal-footer').html('<p id="message"></p><button type="button" class="btn btn-primary" onclick="addSubFolder();">+ Add</button>' +
        '<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>');
    $('#myModal').modal('show');
    console.log(folder_path);
}

function addSubFolder() {
    var folder_name = $('#folder_name_').val();
    if(folder_name =='') {
        $('#message').html('Please enter folder name');
        return false;
    }
    else {
        console.log('folder created successfully');
        var url = "api/folderProcess.php";
        var user_id = $('#user_id').val();

        $.post(url,{name:folder_name,user_id:user_id,parent_id:parent_id,type:'createFolder',folder_path:folder_path},function (data) {
            console.log(JSON.stringify(data));
            var status = data.Status;
            var message = data.Message;

            if(status == 'Success') {
                $("#myModal").modal("hide");
                loadFolderData();
            }
            else if(status =='Failure') {
                $('#message').html(message);
            }
        });
    }
}

function createFolder() {
  var folder_name = $('#folder_name').val();
  if(folder_name =='') {
      $('#messagee').html('Please enter folder name');
      return false;
  }
  else {
      console.log('folder created successfully');
      var url = "api/folderProcess.php";
      var user_id = $('#user_id').val();
      var parent_id = 0;
      $.post(url,{name:folder_name,user_id:user_id,parent_id:parent_id,type:'createFolder'},function (data) {
        console.log(JSON.stringify(data));
        var status = data.Status;
        var message = data.Message;
        if(status == 'Success') {
            window.location.reload();
         }
         else if(status =='Failure') {
            $('#messagee').html(message);
        }
      });
  }
}

function editFolder(id,path,name) {

    // name = decodeURIComponent(name);
    name = decodeURIComponent(name).replace(/\++/g, ' ');
    console.log(name);
    $('.modal-title').html('<label>Edit Folder</label>');
    $('.modal-body').html('<div class="col-md-12"><input type="text" id="folder_name_" placeholder="Please enter folder name" class="form-control" value="'+name+'"/></div>')
    $('.modal-body').css('height','70px');
    $('.modal-footer').html('<p id="message"></p><button type="button" class="btn btn-primary" onclick=updateFolder('+id+',"'+path+'")><i class="fa fa-edit"></i> Edit</button>' +
        '<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>');
    $('#myModal').modal('show');
}

 function updateFolder(id,path) {
   var folder_name_ = $('#folder_name_').val();
   if(folder_name_ == '') {
       $('#message').html('<label>Please enter folder name..</label>');
       $('#message').css('color','red');
   }
   else{
       var url='api/folderProcess.php';
       var user_id = $('#user_id').val();
       path = decodeURIComponent(path).replace(/\++/g, ' ');
       console.log(path);
      $.post(url,{type:'updateFolder','folder_name':folder_name_ ,'folder_path':path,folder_id:id,'user_id':user_id,parent_id:parent_id},function(data) {
        console.log(JSON.stringify(data));
        var status = data.Status;
        if(status == 'Success') {
            $('#myModal').modal('hide');
            setTimeout(function () {
                if(parent_id>0){
                    loadFolderData();
                }
                else
                window.location.reload();
            },1000);
        }
        else{
            $('#message').html('<label>'+data.Message+'</label>');
            $('#message').css('color','red');

        }
      });
   }
 }

function deleteFolder(folder_id,path) {
    $(".modal-content").css({"display":"block"});
    $(".modal-dialog").css({"display":"block"});
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Folder</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
        "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmFolderDelete('"+folder_id+"','"+path+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}

function confirmFolderDelete(id,path) {
    console.log('id --- '+id);
    path = decodeURIComponent(path).replace(/\++/g, ' ');
    var url ='api/folderProcess.php';
    $.post(url,{folder_id:id,folder_path:path,type:'deleteFolder'},function (data) {
      var status = data.Status;
      var message = data.Message;
      if(status == 'Success') {
        // window.location.reload();
          if(parent_id>0) {
              loadFolderData();
          }
          else
              window.location.reload();
      }
      else if(status == 'Failure') {
          $('#message').html('<label>'+message+'</label>');
          $('#message').css('color','red');
      }
     });
}

 function videoOperations(input) {

    $("#uploadbtn").attr("disabled", true);
    var _file = document.getElementById('video_file');
    for (var i = 0; i < _file.files.length; ++i) {
        var file_name = _file.files[i].name;
        var ext = file_name.split(".");
        ext = ext[ext.length - 1].toLowerCase();
        if (ext != "mp4") {
            showMessage("File Extension Should be in MP4 Format Only",'red');
            $("#uploadbtn").attr("disabled", false);
            return false;
        }
    }
    $("#uploadbtn").attr("disabled", false);
}


function editVideo(video_id,video_name) {
    $(".modal-content").css({"display":"block"});
    $(".modal-dialog").css({"display":"block"});
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Edit Video");
    $(".modal-body").css({"display":"block"});
    video_name = unescape(video_name);
    var random = video_name.split(";")[0];
    video_name = video_name.split(";")[1];
    video_name = video_name.split(".mp4")[0];
    $(".modal-body").html("<div class='row'><div class='col-md-6'><div class='form-group'><label>Video Name</label><input type='text'" +
    " value='"+video_name+"' placeholder='Enter Video Name' id='video_name' class='form-control' /></div></div></div>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
    "<input type='button' value='Update' data-dismiss='modal' onclick=updateVideo('"+video_id+"','"+random+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function updateVideo(video_id,random){
    if($("#video_name").val() == ""){
        $("#video_name").css({"border":"1px solid red"});
    }else{
        var video_name = random+";"+$("#video_name").val()+".mp4";
        var url = 'api/videoProcess.php';
        $.post(url,{"type":"renameVideo","video_id":video_id,"video_name":video_name},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                if(parent_id>0){
                    loadFolderData()
                }
                else
                location.reload();
            }else{
                $("#myModal").modal("hide");
            }
        });
    }

}
/*function getSnapShot(filesrc,showClass){
    alert(filesrc);
    var canvas_elem = $( '<canvas style="display:none" ></canvas>' ).appendTo(document.body)[0];
    var $video = $( '<video muted style="display:none" ></video>' ).appendTo(document.body);
    var step_2_events_fired = 0;
    $video.one('loadedmetadata loadeddata suspend', function() {
        if (++step_2_events_fired == 3) {
            $video.one('seeked', function() {
                canvas_elem.height = this.videoHeight;
                canvas_elem.width = this.videoWidth;
                videoDuration = Math.round(this.duration);
                //canvas_elem.getContext('2d').drawImage(this, 0, 0);
                //var snapshot = canvas_elem.toDataURL();
                //videothumb = (snapshot);
                //$("."+showClass).attr("src",snapshot);
                $(".loadNow").css({"display":"none"});
                $(".formbtn").attr("disabled", false);
                // Delete the elements as they are no longer needed
                $video.remove();
                $(canvas_elem).remove();
            }).prop('currentTime','20');
        }
    }).prop('src',filesrc);
}*/
function uploadData() {

    var data = new FormData();
    var video_file = $('#video_file').val();
    var _file = document.getElementById('video_file');
    if(level_id == 3){
        video_file = $('#video_file1').val();
        _file = document.getElementById('video_file1');
        $("#uploadbtn1").attr("disabled", true);
    }
    else{
        $("#uploadbtn").attr("disabled", true);
    }
    if(video_file == ""){
        showMessage("Please Select Atleast One Video File....",'red');
        $("#uploadbtn").attr("disabled", false);
        return false;
    }

    for(var i = 0; i < _file.files.length; ++i){
        data.append('files[]',_file.files[i]);
    }

    data.append('type', "addVideo");
    data.append('video_status', "1");
    data.append('parent_id',parent_id);
    data.append('folder_path',folder_path);
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
        if(request.readyState == 4){
            var response = $.parseJSON(request.response);
            var status = response.Status;
            if(status == "Success"){
                // location.reload(true);
                loadFolderData();
                if(level_id == 3) {
                    $("#uploadbtn1").attr("disabled", false);
                }
                else{
                    $("#uploadbtn").attr("disabled", false);
                }
            }
            else{
                if(level_id == 3) {
                    $("#uploadbtn1").attr("disabled", false);
                }
                else{
                    $("#uploadbtn").attr("disabled", false);
                }
                showMessage(response.Message,'red');
            }
        }
    };
    request.upload.addEventListener('progress', function(e){
        if(level_id == 3){
            $('#percentprog1').html(parseInt((e.loaded/e.total)*100) + '% Complete');
        }
        else{
            $('.percentprog').html(parseInt((e.loaded/e.total)*100) + '% Complete');
        }
    }, false);
    request.open('POST', 'api/videoProcess.php');
    request.setRequestHeader('Cache-Control','no-cache');
    request.send(data);
}
function deleteVideo(video_id) {
    $(".modal-content").css({"display":"block"});
    $(".modal-dialog").css({"display":"block"});
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Video</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
    "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmVideoDelete('"+video_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}

function confirmVideoDelete(video_id){
    var url = "api/videoProcess.php";
    $.post(url,{"type":"deleteVideo","video_id":video_id} ,function (data) {
        var status = data.Status;
        setTimeout(function(){
            if (status == "Success"){
                showMessage(data.Message,"green");
                if(parent_id>0){
                    loadFolderData()
                }
                else
                location.reload(true);
            }
            else{
                showMessage(data.Message,"red");
            }
        },500);
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
}
$(".videoStatus").change(function () {
    if($(this).val() == "0"){
        $(this).val("1");
    }else{
        $(this).val("0");
    }
    var value = $(this).val();
    var id = $(this).attr("id");
    var url = "api/videoProcess.php";
    $.post(url,{"type":"videoStatusChange","value":value,"video_id":id} ,function (data) {
        var status = data.Status;
        if (status == "Success"){
            showMessage(data.Message,"green");
        }
        else{
            showMessage(data.Message,"red");
        }
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
});


/////////////////////////////video part end/////////////////////////////
function addSchedule() {
    window.location='addschedule';
}
function editSchedule(sch_id){
    window.location='editschedule?_id= '+sch_id;
}
var v = 0;
var selectedVideoCount=0;
var startTime = "0";
var startSelection = false;
function toTimestamp(strDate){
    // var datum = Date.parse(strDate);
    var datum = new Date(strDate).getTime();
    // console.log('time -- '+datum.getTime());
    return datum/1000;
}
function getTime() {

    var time = $("#datetimepicker").val();
    var priority = $("#priority").val();
    console.log('priority --- '+priority);
    if(time == '') {
        showMessage('Please select start time first','red');
        return false;
    }

    var url="api/videoProcess.php";
    $.post(url,{"type":"isScheduleExist","timeslot":toTimestamp(time),"priority":priority},function(data) {
        var Status = data.Status;
        if(Status == "Success") {
            if(selectedVideoCount>0) {
                $.post(url,{"type":"reorderTime",'sch_id':sch_id,'start_time':toTimestamp(time)},function(data){
                    var Status = data.Status;
                    if(Status == "Success") {
                        showSelection(sch_id);
                        startTime = data.schedule_end_time;
                        return false;
                    }else{
                        alert(Status);
                    }
                });
            }else{
                startTime = toTimestamp(time);
            }
            startSelection = true;
        }else{
            showMessage(data.Message,'red');

            return false;
        }
    });
}
function selection(row_id,video_id,video_name,video_path){
    video_name = decodeURIComponent(video_name).replace(/\++/g, ' ');
    video_path = decodeURIComponent(video_path).replace(/\++/g, ' ');

    video_path = video_path.substr(video_path.search("ritmu"),video_path.length);
    video_path = 'http://stsmentor.com/'+video_path;
    console.log('path --- '+video_path);
    console.log('name -- '+video_name);

    if($("#datetimepicker").val() == "") {
        showMessage("Please Select Time Slot First",'red');
    }
    else if($("#priority").val() == "") {
        showMessage("Please Select Priority First",'red');
    }
    else {
        if(startSelection) {

            var canvas_elem = $( '<canvas style="display:none" ></canvas>' ).appendTo(document.body)[0];
            var $video = $( '<video muted style="display:none" ></video>' ).appendTo(document.body);
            var step_2_events_fired = 0;
            $video.one('loadedmetadata loadeddata suspend', function() {
                if (++step_2_events_fired == 3) {
                    $video.one('seeked', function() {
                        canvas_elem.height = this.videoHeight;
                        canvas_elem.width = this.videoWidth;
                        videoDuration = Math.round(this.duration);
                        $video.remove();
                        $(canvas_elem).remove();
                    }).prop('currentTime','2');
                }
            }).prop('src',video_path);
            setTimeout(function(){
                var duration = videoDuration;
                var url = "api/videoProcess.php";
                $.post(url, {
                    "type": "storeDuration",
                    "video_id": video_id,
                    "duration": duration,

                }, function (data) {
                    var Status = data.Status;
                    if (Status == "Success") {
                        var priority = $("#priority").val();

                        if(startTime == "") {
                            startTime = toTimestamp($("#datetimepicker").val());
                        }
                        var endTime = parseInt(startTime) + parseInt(duration);
                        $.post(url, {
                            "type": "storeSchedule",
                            "video_id": video_id,
                            "startTime": startTime,
                            "endTime": endTime,
                            "sch_id": sch_id
                        }, function (data) {
                            var Status = data.Status;
                            if (Status == "Success") {
                                startTime = data.endTime;
                                //$("#" + row_id).attr("disabled", true);
                                //$("#" + row_id).val("Selected");
                            } else {
                                showMessage("nnn"+data.Status, 'red');
                            }
                            showSelection(sch_id);
                        });
                    }
                    else {
                        showMessage(data.Status, 'red');
                    }
                    showSelection(sch_id);
                });
            },1000);
        }else{
            showMessage("Please Select Another Time Slot...",'red');
            return false;
        }
    }
}
function clearList() {
    var url = 'api/videoProcess.php';
    $.post(url,{"type":"clearList","sch_id":sch_id},function(data){
       var Status = data.Status;
       if(Status == "Success"){
           $("#datetimepicker").attr("disabled",false);
           startTime = toTimestamp($("#datetimepicker").val());
           showSelection(sch_id);
       }
    });
}
function showSelection(sch_id) {
    var url = "api/videoProcess.php";
    $.post(url,{"type":"getScheduleDetail","sch_id":sch_id},function(data){
        var status = data.Status;
        if (status == "Success") {
            $(".clearList").css("display","block");
            var selectedVideosData = "";
            var videoData = data.sch_data;
            var endTime = 0;

            for(var i=0;i<videoData.length;i++) {

                selectedVideoCount++;
                endTime = videoData[i].end_time;
                var video_name = videoData[i].video_name.split(";")[1];
                video_name = video_name.split(".mp4")[0];
                selectedVideosData = selectedVideosData + "<tr><td>"+(i+1)+"</td><td>" + video_name+"</td>" +
                "<td>" + secondsToMins(videoData[i].video_duration) + " Sec</td><td>" + toHumanFormat(videoData[i].start_time)+
                "</td><td>"+toHumanFormat(videoData[i].end_time)+"</td><td><i onclick=removeSelection('" + videoData[i].detail_id +
                "') class='fa fa-trash'></i></td>*/</tr>";
            }
            $("#selectiontablebody").html(selectedVideosData);
            $("#schedule_end_time").val(toHumanFormat(endTime));
        } else{

            $("#datetimepicker").attr("disabled",false);
            $(".clearList").css("display","none");
            $("#selectiontablebody").html("<tr><td colspan='5' style='text-align:center'>Not Any Videos Selected</td></tr>");
            $("#schedule_end_time").val("");
        }
    });
}
function removeSelection(detail_id){
    var url = 'api/videoProcess.php';
    $.post(url,{'type':'deleteSelection','row_id':detail_id},function(data){
        var Status = data.Status;
        if(Status == "Success"){
            showSelection(sch_id);
            startTime = data.schedule_end_time;
            if(startTime == ""){
                startTime = toTimestamp($("#datetimepicker").val())
            }
        }
    });
}
showSelection(sch_id);
function secondsToMins(seconds){
    var mins = parseInt(seconds/60);
    var secs = parseInt(seconds%60);
    if(mins<10){
        mins = "0"+mins;
    }
    if(secs<10){
        secs = "0"+secs;
    }
    return(mins+":"+secs );
}
function toHumanFormat(unixTime){
    date = new Date(unixTime * 1000);
    var year    = date.getFullYear();
    var month   = date.getMonth();
    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var day     = date.getDate();
    var hour    = date.getHours();
    var ampm = "AM";
    var minute  = date.getMinutes();
    var seconds = date.getSeconds();
    minute = parseInt(minute);
    seconds = parseInt(seconds);
    hour = parseInt(hour);
    if(hour<12){
        ampm = "AM";
    }else if(hour == 12){
        ampm = "PM";
    }else{
        hour = hour-12;
        ampm = "PM";
    }
    if(hour<10){
        hour = "0"+hour;
    }
    if(minute<10){
        minute = "0"+minute;
    }
    if(seconds<10){
        seconds = "0"+seconds;
    }
    return(day+" "+months[month]+" "+year+" "+hour+":"+minute+":"+seconds+" "+ampm);
}
var today = new Date();
var day = today.getFullYear();
var month = today.getMonth();
var year = today.getFullYear();
if(today.getMinutes()>=10){
    var min = 0;
    var hour=today.getHours()+1;
}else{
    var hour = today.getHours();
    var min = 30;
}
$('#datetimepicker').datetimepicker({
    dayOfWeekStart : 1,
    lang:'en',
    value: "",
    step:30,
    format:"d M Y h:i:s A",
    startDate:	day+"/"+month+"/"+year,
    minDate:day+"/"+month+"/"+year
    //defaultTime:hour+":"+min'
});
function createSchedule() {

    var schedule_name = $("#schedule_name").val();
    var schedule_start_time = toTimestamp($("#datetimepicker").val());
    var schedule_end_time = toTimestamp($("#schedule_end_time").val());
    var schedule_desc = $("#schedule_desc").val();
    var schedule_priority = $("#priority").val();

    if(schedule_name == "" || schedule_start_time == "" || schedule_end_time == "" || schedule_desc == "" || schedule_priority == "na"){
        showMessage("all feilds Should be Required Filled",'red');
        return false;
    }
    if(selectedVideoCount < 1) {
        showMessage("Please Select At least One Video",'red');
        return false;
    }
    var url="api/videoProcess.php";
    $.post(url,{"type":"isScheduleExist","timeslot":toTimestamp(schedule_start_time),"priority":schedule_priority},function(data) {
        var Status = data.Status;
        if(Status == "Success") {
            $.post(url,{"type":"createSchedule","sch_name":schedule_name,"sch_start_time":schedule_start_time,
                "sch_end_time":schedule_end_time,"sch_desc":schedule_desc,"sch_id":sch_id,"priority":schedule_priority},function(data){
                var Status = data.Status;
                if(Status == "Success"){
                    window.location="schedule";
                }else{
                    showMessage(data.Message,'red');
                }
            });
         }
         else{
            showMessage(data.Message,'red');
        }

      });


}

function updateSchedule(sch_id) {
    var schedule_name = $("#schedule_name").val();
    var schedule_start_time = toTimestamp($("#datetimepicker").val());
    var schedule_end_time = toTimestamp($("#schedule_end_time").val());
    var schedule_desc = $("#schedule_desc").val();
    var schedule_priority = $("#priority").val();
    if (schedule_name == "" || schedule_start_time == "" || schedule_end_time == "" || schedule_desc == "" || schedule_priority == "na") {
        showMessage("all feilds Should be Required Filled", 'red');
        return false;
    }
    var currentTimeStamp = Math.floor(Date.now() / 1000);
    console.log('end time -- '+toTimestamp($("#schedule_end_time").val())+' curr time '+currentTimeStamp);

    if(schedule_start_time<currentTimeStamp){
        showMessage("Past time Not Selected",'red');
        return false;
    }
    var time = $("#datetimepicker").val();
    var url = "api/videoProcess.php";
    $.post(url, {"type": "isScheduleExist", "timeslot":schedule_start_time,"priority":schedule_priority}, function (data) {
        var Status = data.Status;
        if (Status == "Success") {
            if (selectedVideoCount < 1) {
                showMessage("Please Select Atleast One Video", 'red');
                return false;
            }
            $.post(url, {
            "type": "updateSchedule", "sch_name": schedule_name, "sch_start_time": schedule_start_time,
            "sch_end_time": schedule_end_time, "sch_desc": schedule_desc,"sch_priority":schedule_priority, "sch_id": sch_id
            }, function (data) {
                var Status = data.Status;
                if (Status == "Success") {
                    window.location = "schedule";
                } else {
                    showMessage(data.Message, 'red');
                }
            });
        } else {
            showMessage(data.Message, 'red');
            return false;
        }
    });
}
$(".scheduleStatus").change(function () {
    if($(this).val() == "0"){
        $(this).val("1");
    }else{
        $(this).val("0");
    }
    var value = $(this).val();
    var id = $(this).attr("id");
    var url = "api/videoProcess.php";
    $.post(url,{"type":"scheduleStatusChange","value":value,"sch_id":id} ,function (data) {
        var status = data.Status;
        if (status == "Success"){
            showMessage(data.Message,"green");
        }
        else{
            showMessage(data.Message,"red");
        }
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
});
function delete_sch(sch_id) {
    $(".modal-content").css({"display":"block"});
    $(".modal-dialog").css({"display":"block"});
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Schedule</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
    "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmScheduleDelete('"+sch_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function confirmScheduleDelete(sch_id){
    var url = "api/videoProcess.php";
    $.post(url,{"type":"deleteSchedule","sch_id":sch_id} ,function (data) {
        var status = data.Status;
        setTimeout(function(){
            if (status == "Success"){
                showMessage(data.Message,"green");
                location.reload(true);
            }
            else{
                showMessage(data.Message,"red");
            }
        },500);
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
}
/////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////for change password of admin///////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
function changeAdminPassword(admin_id){
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html('Change Admin Password');
    $(".modal-body").html("<div class='row'><div " +
    "class='col-md-3'></div><div class='col-md-6'><div class='form-group'>" +
    "<label>Old Password</label><input type='password' class='form-control' id='oldPassword' /></div><div class='form-group'><label>" +
    "New Password</label><input type='password' class='form-control' id='newPassword' /></div><div class='form-group'><label>" +
    "Confirm Password</label><input type='password' class='form-control' id='confirmNewPassword' /></div><input type='submit' " +
    "class='btn btn-info pull-right' data-dismiss='modal' onclick=changePassword('"+admin_id+"') value='Change Password'/>" +
    "</div></div><div class='col-md-3'></div>");
    $(".modal-footer").css({"display":"none"});
    $("#myModal").modal("show");
}
function changePassword(admin_id){
    var url = "api/admin_login.php";
    var oldPassword = $("#oldPassword").val();
    var newPassword = $("#newPassword").val();
    var confirmNewPassword = $("#confirmNewPassword").val();
    if(newPassword == confirmNewPassword) {
        setTimeout(function(){
            $.post(url, {
                "type": "changeAdminPassword",
                "old_password": oldPassword,
                "new_password": newPassword,
                "admin_id": admin_id
            }, function (data) {
                var status = data.Status;
                if (status == "Success") {
                    showMessage(data.Message, "green");
                }
                else {
                    showMessage(data.Message, "red");
                }
            }).fail(function () {
                showMessage("Server Error!!! Please Try After Some Time", "red")
            });
        },1000);
    }
    else{
        showMessage("New Password and Confirm Password Should Be Same", "red");
    }
}
function showMessage(message,color){
    $(".modal-header").css({"display":"none"});
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:"+color+"'>"+message+"</span>");
    $(".modal-footer").css({"display":"none"});
    $("#myModal").modal("show");
    setTimeout(function(){
        $("#myModal").modal("hide");
    },2000);
}