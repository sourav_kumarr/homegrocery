/**
 * Created by Sourav on 27-Sep-16.
 */
$(document).ready(function(){
$("#login").submit(function(e){
    e.preventDefault();
    //alert("form clicked");
    var email = $("#email").val();
    var password = $("#password").val();
    var url = "api/user/users/";
    $("#load").removeClass("loader");
    $.ajax({
        url: url,
        type: "GET",
        data: {
            //Set an empty response to see the error
            key:"abc",email:email,password:password,'type':'login'
        },
        dataType:"json",
        success: function(json, textStatus, xhr) {
            //console.log(arguments);
            //console.log(xhr.status);
            $("#load").addClass("loader");
        },
        complete: function(xhr, textStatus) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            $("#load").addClass("loader");
            var status = xhr.status;
            if(status == 200){
              window.location = "/homegrocery/order";
            }
            else if(status == 400){
            var response = xhr.responseText;
            var json = JSON.parse(response);
            var message = json.message;
            alert(message)
            }
        }
    });
  });
});
