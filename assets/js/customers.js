/**
 * Created by Sourav on 01-Oct-16.
 */
$(document).ready(function(){
    $(".btn-container").show();
    $("#main-content").hide();
    $(".leftmenuitems").removeClass("leftmenuitemsactive");
    $("#customers").addClass("leftmenuitemsactive");

    var url = "/homegrocery/api/user/users";
    $.ajax({
        url: url,
        type: "GET",
        data: {
            //Set an empty response to see the error
            key:"abc",type:'all'
        },
        dataType:"json",
        success: function(json, textStatus, xhr) {
            //console.log(arguments);
            //console.log(xhr.status);
            $("#load").addClass("loader");
        },
        complete: function(xhr, textStatus) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            $("#load").addClass("loader");
            var status = xhr.status;
            var response = xhr.responseText;
            var json = JSON.parse(response);

            if(status == 200) {
                //window.location = "/homegrocery/customer";
                var state = json.status;
                var message = json.status;
                var data = json.data;
                $("#main-content").show();
                $(".btn-container").hide();
                var html_data = "<thead><tr><td>#</td><td>Name</td><td>Email</td>"+
                    "<td>Mobile</td><td>Status</td><td>Created At</td></tr></thead><tbody>";

                for(var i=0;i<data.length;i++) {
                    var counter = i+1;
                    var user_state = "";
                    if(data[i].user_status =='A'){
                        user_state = "<select id='user"+i+"' onchange=update_state('"+data[i].user_id+"','user"+i+"')><option value='A' selected>Active</option><option value='D'>Disabled</option></select>";

                    }
                    if(data[i].user_status =='D'){
                        user_state = "<select id='user"+i+"' onchange=update_state('"+data[i].user_id+"','user"+i+"')><option value='A' selected>Active</option><option value='D' selected>Disabled</option></select>";
                    }

                    html_data = html_data+"  <tr><td>"+counter+"</td><td>"+data[i].user_name+"</a></td><td>"+data[i].user_email+"</td>"+
                        "<td>"+data[i].user_mobile+"</td><td>"+user_state+"</td><td>"+data[i].user_date+"</td></tr>";


                }
                $("#state-count").html("Grocery Linked With "+data.length+" Customers");
               $("#example").html(html_data);
            }
            else if(status == 404) {
                response = xhr.responseText;
                json = JSON.parse(response);
                var message = json.message;

                $(".btn-container").html("<label>Sorry No Records found</label>");
            }
        }
    });

});

function update_state(id,viewId) {

 //alert("user--"+id+" state "+$("#"+viewId).val());
    var url = "/homegrocery/api/user/users";
    var items = [];
    items.push({'user_status':$("#"+viewId).val()});
    $(".btn-container").show();
    $(".alert").css("opacity","0");
    $.ajax({
        url: url,
        type: "PUT",
        data: {
            //Set an empty response to see the error
            key:"abc",items:JSON.stringify(items),id:id
        },
        dataType:"json",
        success: function(json, textStatus, xhr) {
            //console.log(arguments);
            //console.log(xhr.status);
        },
        complete: function(xhr, textStatus) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            $(".btn-container").hide();
            var status = xhr.status;
            var response = xhr.responseText;
            var json = JSON.parse(response);

            if(status == 200) {
                //window.location = "/homegrocery/customer";
                var state = json.status;
                var message = json.message;

                $(".alert").html("<span class='closebtn' onclick='close_btn()'>&times;</span> <strong>Success!</strong> Data updated successfully");
                $(".alert").css("opacity",'0.87');
                $('.alert ').removeClass('error');
                $('.alert ').addClass('success');
            }
            else {
                response = xhr.responseText;
                json = JSON.parse(response);
                var message = json.message;

                $(".alert").html(" <span class='closebtn' onclick='close_btn()'>&times;</span><strong>Error!</strong> unable to update record");
                $(".alert").css("opacity",'0.87');
                $('.alert ').removeClass('success');
                $('.alert ').addClass('error');

            }
        }
    });
}


function close_btn(){
    //alert("close is called");
    $(".alert").css("opacity","0");
}
