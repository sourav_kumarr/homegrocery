/**
 * Created by Sourav on 01-Oct-16.
 */

$(document).ready(function(){
    //alert("cats loaded");
    $(".btn-container").hide();
    //$("#main-content").hide();
    $(".leftmenuitems").removeClass("leftmenuitemsactive");
    $("#category").addClass("leftmenuitemsactive");
    //var id = $('#40').parents('ul').addBack().first().attr("id");
    var id = $( "#0 ul:nth-last-child(2)").attr("id");
    alert(JSON.stringify(id));

});


function item_clicked(id) {
    //alert(id);
    $("#"+id).toggle();
    if($("#"+id).is(':visible')){
        //alert("visible");
        $("#i_"+id).removeClass("fa fa-plus");
        $("#i_"+id).addClass("fa fa-minus");
    }
    else{
        //alert("invisible");
        $("#i_"+id).removeClass("fa fa-minus");
        $("#i_"+id).addClass("fa fa-plus");

    }
}

/*
* this will ask to add category..
*
* */

function add_category(title,cat_id,sort_order) {
 //alert("cat "+cat_id+" sort order--"+sort_order);
    var modal_header = "<div class='modal-header' style='padding: 10px'> <span class='close' onclick='close_modal()'>×</span> <label>Add Category</label> </div>";
    var modal_body = "<div class='modal-body'><label>Location</label><select class='form-control' id='loc_id' ><option value='1'>Chandigarh</option><option value='2'>Mohali</option></select></select>" +
        "<label>Category</label><input type='text' id='cat_name' class='form-control' placeholder='enter category name ' style='margin-bottom: 10px'/></div>";

    var modal_footer = "<div class='modal-footer' style='padding: 10px'> <label class='errorr'></label><button class='btn btn-default' onclick=upload_category('"+cat_id+"','"+sort_order+"')>Submit</button>" +
        "<button class='btn btn-default' onclick='close_modal()'>Close</button> </div>";

    var modal_content = "<div class='modal-content'>";
    $("#myModal").html(modal_content+modal_header+modal_body+modal_footer+"</div>");
    $("#myModal").css("display","block");


}

function upload_category(cat_id,sort_order) {
    // alert("upload category is clicked......");

    var cat_name = $("#cat_name").val();
    var loc_id = $("#loc_id").val();
    var snack_bar = document.createElement("div");
    snack_bar.id = 'snackbar';

    if(cat_name == '') {

         $(".errorr").html("please enter all fields..");
         $(".errorr").css("display","block");
        //return ;
    }
    else {
        var url = "/homegrocery/api/category/items";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                //Set an empty response to see the error
                key:"abc",cat_id:cat_id,'sort_id':sort_order,'parent_id':cat_id,'cat_name':cat_name,'loc_id':loc_id
            },
            dataType:"json",
            success: function(json, textStatus, xhr) {
                //console.log(arguments);
                //console.log(xhr.status);
            },
            complete: function(xhr, textStatus) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                var status = xhr.status;
                var response = xhr.responseText;
                var json = JSON.parse(response);

                if(status == 200) {
                    //window.location = "/homegrocery/customer";
                    $(".errorr").html("category successfully added");
                    $(".errorr").css("display","block");
                    var data = json.data;
                    setTimeout(function(){
                        $(".errorr").css("display","none");
                        $(".order-container").html(data);
                        $("#myModal").css("display","none");
                        //$("#"+cat_id).css("display","block");
                        //var id = $("#"+cat_id).parents('ul:first').attr('id');
                        var id = $("#"+cat_id).parents('ul:first').attr('id');
                        alert(cat_id+"+"+id);
                        item_clicked(id);



                    },2000);

                }
                else if(status == 404) {

                    response = xhr.responseText;
                    json = JSON.parse(response);
                    var message = json.message;
                    $(".errorr").html("please enter all fields..");
                    $(".errorr").css("display","block");

                }
            }
        });
    }

}

function edit_category(cat_name,cat_id,sort_order) {
  // alert(cat_id);
    cat_name = decodeURIComponent((cat_name + '').replace(/\+/g, '%20'));
    //alert(cat_name);
    var modal_header = "<div class='modal-header' style='padding: 10px'> <span class='close' onclick='close_modal()'>×</span> <label>Edit Category</label> </div>";
    var modal_body = "<div class='modal-body'><label>Location</label><select class='form-control' id='loc_id' ><option value='1'>Chandigarh</option><option value='2'>Mohali</option></select></select>" +
        "<label>Category</label><input type='text' id='cat_name' class='form-control' placeholder='enter category name ' value='"+cat_name+"' style='margin-bottom: 10px'/></div>";

    var modal_footer = "<div class='modal-footer' style='padding: 10px'> <label class='errorr'></label><button class='btn btn-default' onclick=upload_category('"+cat_id+"','"+sort_order+"')>Submit</button>" +
        "<button class='btn btn-default' onclick='close_modal()'>Close</button> </div>";

    var modal_content = "<div class='modal-content'>";
    $("#myModal").html(modal_content+modal_header+modal_body+modal_footer+"</div>");
    $("#myModal").css("display","block");


}
function close_modal() {
    $("#myModal").css("display","none");
}
