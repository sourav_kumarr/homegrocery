/**
 * Created by Sourav on 28-Sep-16.
 */

$(document).ready(function()
{
    var shipped = $('#shippedcount').val();
    var total = $('#totalcount').val();
    var processing = total-shipped;
    $("#processing").html(processing);
    $("#completed").html(shipped);
    $(".leftmenuitems").removeClass("leftmenuitemsactive");
    $("#index").addClass("leftmenuitemsactive");

    var url = "/homegrocery/api/orders/items/";

    $("#loader").css("display","block");
    //alert("hello");
    $.ajax({
        url: url,
        type: "GET",
        data: {
            //Set an empty response to see the error
            key:"abc",type:'all'
        },
        dataType:"json",
        success: function(json, textStatus, xhr) {
            //console.log(arguments);
            //console.log(xhr.status);
        },
        complete: function(xhr, textStatus) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            $("#loader").css("display","none");
            var status = xhr.status;
            var response = xhr.responseText;
            var json = JSON.parse(response);

            if(status == 200) {
              var order_items = "<table class='table table-bordered table-hover' id='example'><thead><tr><td>#</td><td>Number</td><td>Total</td>" +
                  "<td>Payment Status</td><td>Status</td><td>Created at</td><td>Created By</td></tr></thead><tbody>";
              var data = json.data;
              var ship_count = 0;
              for(var i=0;i<data.length;i++) {
                 var counter = i+1;
                 var order_state = data[i].order_state;
                 if(order_state == "open") {
                     //order_state = "OPEN";
                     order_state = "<select id='order"+i+"' onchange=update_state('"+data[i].order_id+"','order"+i+"')><option value='open' selected>Open</option><option value='ship'>Shipped</option>" +
                         "<option value='complete'>Completed</option><option value='cancel'>Cancelled</option></select>";
                 }
                 else if(order_state == "ship"){
                     order_state = "<select id='order"+i+"' onchange=update_state('"+data[i].order_id+"','order"+i+"')><option value='open' >Open</option><option value='ship' selected>Shipped</option>" +
                         "<option value='complete'>Completed</option><option value='cancel'>Cancelled</option></select>";
                     ship_count = ship_count+1;
                 }
                 else if(order_state == "complete"){
                     order_state = "<select id='order"+i+"' onchange=update_state('"+data[i].order_id+"','order"+i+"')><option value='open' >Open</option><option value='ship' >Shipped</option>" +
                         "<option value='complete' selected>Completed</option><option value='cancel'>Cancelled</option></select>";

                 }
                 else if(order_state == "cancel"){
                     order_state = "<select id='order"+i+"' onchange=update_state('"+data[i].order_id+"','order"+i+"')><option value='open' >Open</option><option value='ship' >Shipped</option>" +
                         "<option value='complete' >Completed</option><option value='cancel' selected>Cancelled</option></select>";

                 }

                 order_items = order_items + "<tr><td>"+counter+"</td><td><a href='/homegrocery/order/view/"+data[i].order_id+"'>"+data[i].order_number+"</a></td><td>"+data[i].order_total+"</td>" +
                     "<td>"+data[i].order_payment_status+"</td><td>"+order_state+"</td><td>"+data[i].order_date+"</td><td>"+data[i].user_name+"</td></tr>";
              }
                $(".ordercount").html(data.length);
                $("#completed").html(ship_count);
                $("#dynamic_table").append(order_items+"</tbody></table>");
            }
            else if(status == 400) {
                var message = json.message;
                $(".btn-container").html("<label style='color: red'>"+message+"</label>");
            }
            //$('#example').DataTable();

        }
    });
    $(".closebtn").click(function(){
        $(".alert").css("opacity","0");

    })

});

function update_state( order_id ,id ) {
    //alert(order_id);
    var url = "/homegrocery/api/orders/items/";
    var state = $("#"+id).val();
    var items = [];
    items.push({'order_state':state});
    //alert(state);
    $("#loader").css("display","block");

    $.ajax({
        url: url,
        type: "PUT",
        data: {
            //Set an empty response to see the error
            key: "abc", id: order_id,"items":JSON.stringify(items)
        },
        dataType: "json",
        success: function (json, textStatus, xhr) {
            //console.log(arguments);
            //console.log(xhr.status);

        },
        complete: function (xhr, textStatus) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            $("#loader").css("display","none");
            var status = xhr.status;
            if(status == 200){
                $("#success").html(" <span class='closebtn' onclick='close_btn()'>&times;</span><strong>Success!</strong> updated successfully");
                $("#success").css("opacity",'0.87');
                var json = JSON.parse(xhr.responseText);
                var data = json.data;
                if(data)
                $("#completed").html(data.length);
                else
                    $("#completed").html("0");


            }
            else if(status == 400) {
                $("#error").html(" <span class='closebtn' onclick='close_btn()'>&times;</span><strong>Error!</strong> unable to update record");
                $("#error").css("opacity",'0.87');

            }
            else if(status == 403) {
                $("#error").html(" <span class='closebtn' onclick='close_btn()'>&times;</span><strong>Error!</strong> unable to update record");
                $("#error").css("opacity",'0.87');

            }

        }
    });


}

function close_btn(){
    $(".alert").css("opacity","0");
}
