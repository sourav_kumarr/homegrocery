/**
 * Created by Sourav on 30-Sep-16.
 */
function body_loaded(type) {
    var url = '/homegrocery/api/orders/items/';
    $("#main-content").hide();
    $.ajax({
        url: url,
        type: "GET",
        data: {
            //Set an empty response to see the error
            key:"abc",type:'state','state':type
        },
        dataType:"json",
        success: function(json, textStatus, xhr) {
            //console.log(arguments);
            //console.log(xhr.status);
        },
        complete: function(xhr, textStatus) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            $("#loader").css("display","none");
            var status = xhr.status;
            var response = xhr.responseText;
            var json = JSON.parse(response);

            if(status == 200) {
             var state = json.status;
             var message = json.message;
             var data = json.data;
             if(state){
                 $(".btn-container").hide();
                 $("#main-content").show();
                 var order_state = "";
                 var html_data = "<thead><tr><td>#</td><td>Number</td><td>Total</td>"+
                     "<td>Payment Status</td><td>State</td><td>Created At</td><td>Created By</td>"+
                     "</tr></thead><tbody>";
                 if(type == 'complete'){
                     order_state = "<i class='fa fa-check-circle-o completed'></i>";
                     $("#state").html("<i class='fa fa-check-circle-o fa-4x fa completed'></i>");
                     $("#in-process").html("In-Complete Orders");
                     $("#state-count").html(data.length+" Order Completed");

                 }
                 else if(type == 'ship'){
                     order_state = "<i class='fa fa-truck ship'></i>";
                     $("#state").html("<i class='fa fa-truck fa-4x ship'></i>");
                     $("#in-process").html("In-Ship Orders");
                     $("#state-count").html(data.length+" Order to be shipped");

                 }
                 else if(type == 'open'){
                     order_state = "<i class='fa fa fa-clock-o process'></i>";
                     $("#state").html("<i class='fa fa fa-clock-o fa-4x process'></i>");
                     $("#in-process").html("In-Process Orders");
                     $("#state-count").html(data.length+" Order taken");

                 }
                 else if(type == 'cancel'){
                     order_state = "<i class='fa fa-remove cancel'></i>";
                     $("#state").html("<i class='fa fa-remove fa-4x cancel'></i>");
                     $("#in-process").html("In-Cancel Orders");
                     $("#state-count").html(data.length+" Order Canceled");

                 }

                 for(var i = 0;i<data.length;i++) {
                   var counter = i+1;
                   html_data = html_data+"  <tr><td>"+counter+"</td><td><a href='/homegrocery/order/view/"+data[i].order_id+"'>"+data[i].order_number+"</a></td><td>"+data[i].order_total+"</td><td>"+data[i].order_payment_status+"</td><td>"+order_state+"</td>"+
                       "<td>"+data[i].order_date+"</td><td>"+data[i].user_name+"</td></tr>";
                 }
                 $("#example").html(html_data);
             }
             else{
                 $(".btn-container").html("<label style='color: red'>"+message+"</label>");

             }
            }
            else if(status == 400) {
                var message = json.message;
                $(".btn-container").html("<label style='color: red'>"+message+"</label>");
            }
            //$('#example').DataTable();

        }
    });
}
